<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
|
|   ROUTES FRONT
 */

$route['default_controller']            =   'front';
$route['404_override']                  =   'front/error_404';
$route['translate_uri_dashes']          =   FALSE;

/* ROUTES FRONT */
$route['servicios']                     =   'front/servicio';
$route['servicios/(:any)']              =   'front/servicios/$1';
$route['nosotros']                      =   'front/nosotro';
$route['nosotros/(:any)']               =   'front/nosotros/$1';
$route['faq']                           =   'front/faq';
$route['contactenos']                   =   'front/contactenos';
$route['necesitas-ayuda']               =   'front/necesitas_ayuda';
$route['publicaciones']                 =   'front/publicaciones';

/* ROUTES BACK */

$toute['manager/migrate/down']          =   'migrate/down';
$route['manager/migrate']               =   'migrate';

$route['manager/login']                 =   'user/login';
$route['manager/logout']                =   'user/logout';

$route['manager/dashboard']             =   'dashboard';
$route['manager/password']              =   'dashboard/change';
$route['manager/password/update']       =   'dashboard/change_pwd';

$route['manager/servicios']             =   'dashboard/servicios';
$route['manager/servicios/editar/(:num)'] =   'dashboard/seditar/$1';
$route['manager/servicios/create']     =   'dashboard/screate/';

$route['manager/analisis']              =   'dashboard/analisis';
$route['manager/analisis/editar/(:num)']=   'dashboard/aeditar/$1';
$route['manager/analisis/create']       =   'dashboard/acreate';

$route['manager/grupo_analisis']              =   'dashboard/grupo';
$route['manager/grupo_analisis/editar/(:num)']=   'dashboard/geditar/$1';
$route['manager/grupo_analisis/create']       =   'dashboard/gcreate';

// galeria de productos
$route['manager/galeria']               =   'dashboard/galeria';
$route['manager/galeria/create']        =   'dashboard/gcreate';
$route['manager/galeria/delete/(:num)'] =   'dashboard/gdelete/$1';