<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// RECUPERANDO ESTADO
// 1: ACTIVO
// 0: INACTIVO
if ( ! function_exists('get_estado')){
    function get_estado($estado){
        if($estado == 1)
            return '<span class="label label-success">Activo</span>';
        else
            return '<span class="label label-danger">No Activo</span>';
    }
}

// RECUPERANDO TIPO
// 1: EMPAQUETADO
// 2: PERSONALIZADO
if ( ! function_exists('get_tipo')){
    function get_tipo($estado){
        if($estado == 1)
            return 'Empaquetado';
        else
            return 'Personalizado';
   }
}

// EXTRACTO DE UN TEXTO LARGO
if ( ! function_exists('get_extracto_texto')){
    function get_extracto_texto($texto, $caracteres){
        return substr($texto, 0 , $caracteres).'...';
    }
}

// RECUPERANDO SERVICIO
if ( ! function_exists('get_servicio')){
    function get_servicio($id){
        //get main CodeIgniter object
        $ci =& get_instance();
        //load databse library
        $ci->load->database();
        //get data from database
        $query = $ci->db->get_where('servicio',array('idServicio'=>$id));
        if($query->num_rows() > 0){
            $result = $query->row_array();
            return $result['nombre'];
        }else{
            return false;
        }
   }
}

// RECUPERANDO GRUPO DE ÁNALISIS
if ( ! function_exists('get_grupo')){
    function get_grupo($id){
        //get main CodeIgniter object
        $ci =& get_instance();
        //load databse library
        $ci->load->database();
        //get data from database
        $query = $ci->db->get_where('grupo_analisis',array('idGrupo_Analisis'=>$id));
        if($query->num_rows() > 0){
            $result = $query->row_array();
            return $result['nombre'];
        }else{
            return '-';
        }
   }
}

// RECUPERANDO IMAGENES SERVICIO
if ( ! function_exists('get_img_servicio')){
    function get_img_servicio($id){
        //get main CodeIgniter object
        $ci =& get_instance();
        //load databse library
        $ci->load->database();
        //get data from database
        $query = $ci->db->get_where('imagen',array('idImagen'=>$id));
        if($query->num_rows() > 0){
            $result = $query->row_array();
            return $result['url'];
        }else{
            return false;
        }
   }
}

// RECUPERANDO ESTADO FORMULARIO
if ( ! function_exists('get_estado_form')){
    function get_estado_form($estado){
        if($estado == 0){
            $input =    '<label class="btn btn-danger btn-sm active">';
            $input .=       '<input type="radio" name="estado" value="0" checked>';
            $input .=       '<i class="glyphicon glyphicon-remove" checked></i> No activo';
            $input .=   '</label> ';
            $input .=   '<label class="btn btn-success btn-sm">';
            $input .=       '<input type="radio" name="estado" value="1">';
            $input .=       '<i class="glyphicon glyphicon-ok"></i> Activo';
            $input .=   '</label>';
            return $input;
        }
        if($estado == 1){
            $input =    '<label class="btn btn-danger btn-sm">';
            $input .=       '<input type="radio" name="estado" value="0">';
            $input .=       '<i class="glyphicon glyphicon-remove"></i> No activo';
            $input .=   '</label> ';
            $input .=   '<label class="btn btn-success btn-sm active">';
            $input .=       '<input type="radio" name="estado" value="1" checked>';
            $input .=       '<i class="glyphicon glyphicon-ok"></i> Activo';
            $input .=   '</label>';
            return $input;
        }
        if($estado == 3){
            $input =   '<label class="btn btn-success btn-sm">';
            $input .=       '<input type="radio" name="estado" value="1">';
            $input .=       '<i class="glyphicon glyphicon-ok"></i> Activo';
            $input .=  '</label> ';
            $input .=    '<label class="btn btn-danger btn-sm">';
            $input .=       '<input type="radio" name="estado" value="0">';
            $input .=       '<i class="glyphicon glyphicon-remove"></i> No activo';
            $input .=   '</label>';
            return $input;
        }
    }
}

// RECUPERANDO ESTADO FORMULARIO
if ( ! function_exists('get_tipo_form')){
    function get_tipo_form($tipo){
        if($tipo == 1){
            $input =    '<label class="btn btn-danger btn-sm active">';
            $input .=       '<input type="radio" name="tipo" value="1" checked>';
            $input .=       '<i class="glyphicon glyphicon-remove" checked></i> Empaquetado';
            $input .=   '</label> ';
            $input .=   '<label class="btn btn-success btn-sm">';
            $input .=       '<input type="radio" name="tipo" value="2">';
            $input .=       '<i class="glyphicon glyphicon-ok"></i> Adicionales';
            $input .=   '</label>';
            return $input;
        }
        if($tipo == 2){
            $input =    '<label class="btn btn-danger btn-sm">';
            $input .=       '<input type="radio" name="tipo" value="1">';
            $input .=       '<i class="glyphicon glyphicon-remove"></i> Empaquetado';
            $input .=   '</label> ';
            $input .=   '<label class="btn btn-success btn-sm active">';
            $input .=       '<input type="radio" name="tipo" value="2" checked>';
            $input .=       '<i class="glyphicon glyphicon-ok"></i> Adicionales';
            $input .=   '</label>';
            return $input;
        }
        if($tipo == 3){
            $input =    '<label class="btn btn-danger btn-sm">';
            $input .=       '<input type="radio" name="tipo" value="1">';
            $input .=       '<i class="glyphicon glyphicon-remove"></i> Empaquetado';
            $input .=   '</label> ';
            $input .=   '<label class="btn btn-success btn-sm">';
            $input .=       '<input type="radio" name="tipo" value="2">';
            $input .=       '<i class="glyphicon glyphicon-ok"></i> Adicionales';
            $input .=   '</label>';
            return $input;
        }
    }
}