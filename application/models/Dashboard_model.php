<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User_model class.
 *
 * @extends CI_Model
 */
class Dashboard_model extends CI_Model {

    /**
     * __construct function.
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
     * [lst listado de tablas]
     * @param  [string] $table  [nombre de tabla]
     */
    public function lst($table)
    {
        $query = $this->db->get($table);
        if($query->num_rows() > 0){
            return $query->result();
        }
    }

    /**
     * [lst_get listado de tablas]
     * @param  [string] $table  [nombre de tabla]
     */
    public function lst_get($table, $id_table ,$id)
    {
        $query = $this->db->get_where($table, array($id_table => $id));
        if($query->num_rows() > 0){
            return $query->result();
        }
    }


    /**
     * [lst listado de tablas]
     * @param  [string]     $table  [nombre de tabla]
     * @param  [string]     $slug   [slug de idioma]
     */
    public function lst_country($table, $country)
    {
        $query = $this->db->get($table);
        if($query->num_rows() > 0){
            return $query->result();
        }
    }

    /**
     * [delete eliminar dato]
     * @param  [string]     $table  [nombre de tabla]
     * @param  [int]        $id     [id de item a aeliminar]
     * @return [type]               [description]
     */
    public function delete($table, $id_table, $id)
    {
        $this->db->where($id_table, $id);
        return $this->db->delete($table);
    }

    /**
     * [create crear nuevo dato]
     * @param  [strgin] $table      [nombre de table]
     * @param  [array]  $data       [datos a insertar]
     * @return [type]               [description]
     */
    public function create($table, $data)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    /**
     * [get recuperar dato por id]
     * @param  [string] $table      [nombre de tabla]
     * @param  [int]    $id         [id de consulta]
     * @return [type]               [description]
     */
    public function get($table, $tabla_id, $user_id) {
        $this->db->from($table);
        $this->db->where($tabla_id, $user_id);
        return $this->db->get()->row();
    }

    /**
     * [sql sentencia sql]
     * @param  [string] $sql      [sentencia sql]
     */
    public function sql($sql) {
        $query = $this->db->query($sql);
        return $query->result();
    }

    /**
     * [get_row recuperar dato por id]
     * @param  [string] $table      [nombre de tabla]
     * @param  [int]    $id         [id de consulta]
     * @return [type]               [description]
     */
    public function get_row($table, $id) {

        $this->db->from($table);
        $this->db->where('id', $id);
        return $this->db->get()->row();

    }

    /**
     * [update actualizar]
     * @param  [string] $table      [nombre de tabla]
     * @param  [array]  $data       [nombre de tabla]
     * @param  [int]    $id         [id de consulta]
     * @return [array]              [description]
     */
    public function update($table, $data, $idTable, $id)
    {
        $this->db->where($idTable, $id);
        return $this->db->update($table, $data);
    }

}
