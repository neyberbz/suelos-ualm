<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User_model class.
 *
 * @extends CI_Model
 */
class Country_model extends CI_Model {

    /**
     * __construct function.
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function lst($table)
    {
        $query = $this->db->get($table);
        if($query->num_rows() > 0){
            return $query->result();
        }
    }

    public function create($table, $data)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function get($table, $id) {

        $this->db->from($table);
        $this->db->where('id', $id);
        return $this->db->get()->result();

    }

    public function get_row($table, $id) {

        $this->db->from($table);
        $this->db->where('id', $id);
        return $this->db->get()->row();

    }

}
