<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User_model class.
 *
 * @extends CI_Model
 */
class Front_model extends CI_Model {

    /**
     * __construct function.
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library(array('session','form_validation','image_lib'));
        $this->load->helper(array('url','form','extras'));
        $this->load->model('dashboard_model');
        $this->load->model('front_model');
    }

    /**
     * [recovery_country recuperar slug de idioma]
     */
    public function recovery_country($slug){
        $this->db->from('oda_idiomas');
        $this->db->where('slug', $slug);
        return $this->db->get()->row();
    }

    /**
     * [lst_nosotros description]
     * @param  [type] $key_id       [description]
     * @param  [type] $id_categoria [description]
     * @return [type]               [description]
     */
    public function get_nosotros($key_id, $id_categoria)
    {
        $this->db->from('oda_post');
        $this->db->where('id_categoria', $id_categoria);
        $this->db->where('key_id', $key_id);
        return $this->db->get()->row();
    }


    public function recovery_categoria($id_country, $key_id)
    {

        $this->db->select('*');
        $this->db->from('oda_post p');
        $this->db->join('oda_categoria c', 'c.id = p.id_categoria');
        $this->db->where('p.key_id', $key_id);
        $this->db->where('c.id_idioma', $id_country);
        $query = $this->db->get();
        return $query->row();
    }

    /**
     * [slider description]
     * @param  [type] $country [description]
     * @return [type]          [description]
     */
    public function slider($country)
    {
        $this->db->from('oda_slider');
        $this->db->where('id_idioma', $country);
        $this->db->order_by('orden');
        return $this->db->get()->result();
    }

    /**
     * [slider_int description]
     * @param  [type] $country [description]
     * @return [type]          [description]
     */
    public function slider_int($country)
    {
        $this->db->from('oda_slider');
        $this->db->where('id_idioma', $country);
        $this->db->order_by('orden');
        return $this->db->get()->result();
    }

    /**
     * [lst description]
     * @param  [type] $table [description]
     * @return [type]        [description]
     */
    public function lst($table)
    {
        $query = $this->db->get($table);
        if($query->num_rows() > 0){
            return $query->result();
        }
    }

    /**
     * [create description]
     * @param  [type] $table [description]
     * @param  [type] $data  [description]
     * @return [type]        [description]
     */
    public function create($table, $data)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    /**
     * [get description]
     * @param  [type] $table [description]
     * @param  [type] $id    [description]
     * @return [type]        [description]
     */
    public function get($table, $id) {

        $this->db->from($table);
        $this->db->where('id', $id);
        return $this->db->get()->result();

    }

    /**
     * [get_row description]
     * @param  [type] $table [description]
     * @param  [type] $id    [description]
     * @return [type]        [description]
     */
    public function get_master($table, $id) {
        $this->db->from($table);
        $this->db->where('key', $id);
        $query = $this->db->get();
        return $query->result();
    }

}
