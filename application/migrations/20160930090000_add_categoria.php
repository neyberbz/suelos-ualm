<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_categoria extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'nombre' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100'
                        ),
                        'descripcion' => array(
                                'type' => 'TEXT',
                                'null' => TRUE
                        ),
                        'img' => array(
                                'type' => 'VARCHAR',
                                'constraint' => 11,
                                'value' => 'img-oda.jpg'
                        ),
                        'controllers' => array(
                                'type' => 'VARCHAR',
                                'constraint' => 200
                        ),
                        'id_idioma' => array(
                                'type' => 'INT',
                                'constraint' => 11
                        ),
                        'parent' => array(
                                'type' => 'INT',
                                'constraint' => 11
                        ),
                        'created_at' => array(
                                'type' => 'DATE'
                        ),
                        'updated_at' => array(
                                'type' => 'DATE'
                        ),
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('oda_categoria');
        }

        public function down()
        {
                $this->dbforge->drop_table('oda_categoria');
        }
}