
        <div class="panel panel-default">

            <div class="panel-heading">
                <div class="panel-title">
                    Cambiar contraseña
                </div>
            </div>

            <div class="panel-body">

                <?php if (validation_errors()) : ?>
                <div class="alert alert-danger" role="alert">
                    <?= validation_errors() ?>
                </div>
                <?php endif; ?>

                <?php
                $attributes = array('role' => 'form');
                echo form_open_multipart('manager/password/update', $attributes);
                ?>

                    <input type="hidden" name="id" value="<?= $get->id ?>" required="required">
                    <input type="hidden" name="id_idioma" value="<?= $get->id_pais ?>" required="required">

                    <div class="form-group form-group-default required ">
                        <label>Nueva contraseña</label>
                        <input type="password" class="form-control" name="password" placeholder="Ingresar nueva contraseña...." required>
                    </div>

                    <button type="submit" class="btn btn-success">
                        <span>Actualizar contraseña</span>
                    </button>

                <?php
                echo form_close();
                ?>

            </div>

        </div>