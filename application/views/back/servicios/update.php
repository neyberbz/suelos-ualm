 <?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

        <!-- MAIN -->
        <main>
            <div class="container">
                <div class="col-md-12 tabs">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">
                            <ul class="nav nav-tabs nav-justified">
                                <li class="active">
                                    <a href="#servicio" aria-controls="servicio" role="tab" data-toggle="tab">
                                        <?= $seccion_title ?>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <hr style="width: 100%; display: block; float: left; margin-top: 0;">
                        <div class="col-md-12 view-item">
                            <div class="tab-content">

                                <div role="tabpanel" class="tab-pane fade in active" id="servicio">

                                    <?php
                                    $attributes = array('role' => 'form');
                                    echo form_open_multipart('dashboard/supdate', $attributes);
                                    ?>
                                        <input type="hidden" name="idServicio" value="<?= $get->idServicio ?>">

                                        <div class="form-group required">
                                            <label for="nombre">Nombre</label>
                                            <input type="text" name="nombre" class="form-control" id="nombre" value="<?= $get->nombre ?>">
                                            <p class="help-block">
                                                <strong>URL de Servicio: </strong>
                                                <a href="<?= base_url('servicios/').$get->slug ?>" target="_blank">
                                                    <?= base_url('servicios/').$get->slug ?>
                                                </a>
                                            </p>
                                        </div>
                                        <div class="form-group required">
                                            <label for="descripcion">Descripción</label>
                                            <textarea name="descripcion" class="form-control" rows="4"><?= $get->descripcion ?></textarea>
                                        </div>
                                        <div class="form-group required">
                                            <label for="npta">Nota</label>
                                            <textarea name="nota" class="form-control summernote"><?= $get->nota ?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <input type="hidden" name="imagen_portada" value="<?= $get->idImagen ?>" >
                                            <label for="imagen-portada">Imagen de Portada</label>
                                            <input type="file" name="userfile" id="imagen-portada">
                                            <p class="help-block">Esta imagen se mostrara en la portada. Tamaño 397 x 427 </p>
                                            <img src="<?= base_url('uploads/servicios/'.$get->idServicio.'/'.get_img_servicio($get->idImagen)) ?>" class="img-responsive" width="100">
                                        </div>
                                        <div class="form-group">
                                            <input type="hidden" name="imagen_contenido" value="<?= $get->idImagenPortada ?>" >
                                            <label for="imagen-contenido">Imagen de Contenido</label>
                                            <input type="file" name="userfile1" id="imagen-contenido">
                                            <p class="help-block">Esta imagen se mostrara en la parte informativa del servicio. Tamaño 857 x 340</p>
                                            <img src="<?= base_url('uploads/servicios/'.$get->idServicio.'/'.get_img_servicio($get->idImagenPortada)) ?>" class="img-responsive" width="100">
                                        </div>
                                        <div class="form-group">
                                            <label for="estado">Estado</label>
                                            <div data-toggle="buttons">
                                                <?= get_estado_form($get->estado) ?>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary pull-left">Actualizar servicio</button>
                                        <a href="<?= base_url('manager/servicios') ?>" class="btn btn-primary pull-right">Regresar a Listado</a>
                                    <?php
                                    echo form_close();
                                    ?>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <!-- /MAIN -->