 <?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

        <!-- MAIN -->
        <main>
            <div class="container">
                <div class="col-md-12 tabs">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">
                            <ul class="nav nav-tabs nav-justified">
                                <li class="active">
                                    <a href="#servicio" aria-controls="servicio" role="tab" data-toggle="tab">
                                        <?= $seccion_title ?>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <hr style="width: 100%; display: block; float: left; margin-top: 0;">
                        <div class="col-md-12 view-item">
                            <div class="tab-content">

                                <div role="tabpanel" class="tab-pane fade in active" id="servicio">

                                    <?php
                                    $attributes = array('role' => 'form');
                                    echo form_open_multipart('dashboard/aupdate', $attributes);
                                    ?>
                                        <input type="hidden" name="id" id="id" value="<?= $get->idAnalisis ?>">

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="nombre">Servicio</label>
                                                    <?php
                                                    $servicios[''] = 'Seleccionar servicio...';
                                                    foreach ($lst_servicios as $item):
                                                        $servicios[$item->idServicio] = $item->nombre;
                                                    endforeach;
                                                    $css = array('class' => 'form-control', 'required' => 'required');
                                                    echo form_dropdown('servicios', $servicios, $get->idServicio, $css);
                                                    ?>
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="nombre">Grupo de Analisis</label>
                                                    <?php
                                                    $grupoAnalisis[''] = 'Seleccionar Grupo de Ánalisis...';
                                                    foreach ($lst_analisis as $item):
                                                        $grupoAnalisis[$item->idGrupo_Analisis] = $item->nombre;
                                                    endforeach;
                                                    $css = array('class' => 'form-control');
                                                    echo form_dropdown('grupoAnalisis', $grupoAnalisis, $get->idGrupo_Analisis, $css);
                                                    ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="nombre">Nombre</label>
                                            <input type="text" name="nombre" class="form-control" id="nombre" value="<?= $get->nombre ?>" required>
                                        </div>

                                        <div class="form-group required">
                                            <label for="descripcion">Descripción</label>
                                            <textarea name="descripcion" class="form-control" rows="2" placeholder="Ingresar descripcion"><?= $get->descripcion ?></textarea>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label for="costo">Costo</label>
                                                    <input type="text" name="costo" class="form-control" id="costo" value="<?= $get->precio ?>">
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="tipo">Tipo</label>
                                                    <div data-toggle="buttons">
                                                        <?= get_tipo_form($get->tipo) ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="estado">Estado</label>
                                                    <div data-toggle="buttons">
                                                        <?= get_estado_form($get->estado) ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button type="submit" class="btn btn-primary pull-left">Actualizar Datos de Ánalisis</button>
                                                    <a href="<?= base_url('manager/analisis') ?>" class="btn btn-primary pull-right">Regresar a Listado</a>
                                                </div>
                                            </div>
                                        </div>

                                        <hr>
                                        <br>

                                        <div class="form-group">
                                            <div class="row" id="formElementos">
                                                <div class="col-md-12">
                                                    <h6><strong>NUEVO ELEMENTO</strong></h6>
                                                </div>
                                                <input type="hidden" name="idAnalisis" id="idAnalisis" value="<?= $get->idAnalisis ?>">
                                                <input type="hidden" name="idServicio" id="idServicio" value="<?= $get->idServicio ?>">
                                                <input type="hidden" name="urlElemento" id="urlElemento" value="<?= base_url('dashboard/elementoAdd') ?>">
                                                <input type="hidden" name="urlDeleteElemento" id="urlDeleteElemento" value="<?= base_url('dashboard/elementoDelete/') ?>">
                                                <div class="col-md-3">
                                                    <label>Nombre</label>
                                                    <input type="text" class="form-control" name="nombre_elemento" id="nombre_elemento" placeholder="Nombre de Elemento">
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Descripción</label>
                                                    <input type="text" class="form-control" name="descripcion_elemento" id="descripcion_elemento" placeholder="Descripción de Elemento">
                                                </div>
                                                <div class="col-md-1">
                                                    <label>Precio</label>
                                                    <input type="text" class="form-control" name="precio_elemento" id="precio_elemento" placeholder="Precio">
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Estado</label>
                                                    <div data-toggle="buttons">
                                                        <?= get_estado_form(3) ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <button type="button" class="m-t-25 btn btn-primary pull-left" id="addElemento">Agregar</button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="table-responsive">
                                                <table class="table table1" data-page-length="10" id="tableElementos">
                                                    <caption>Listado de Elementos</caption>
                                                    <thead>
                                                        <tr>
                                                            <th>Cod.</th>
                                                            <th>Nombre</th>
                                                            <th>Descripción</th>
                                                            <th>Precio</th>
                                                            <th>Estado</th>
                                                            <th>Eliminar</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    foreach($lst_elementos as $item):
                                                    ?>
                                                        <tr class="alert">
                                                            <td class="text-center">
                                                                <?= $item->idElemento ?>
                                                            </td>
                                                            <td>
                                                                <?= $item->nombre ?>
                                                            </td>
                                                            <td>
                                                                <?= $item->descripcion ?>
                                                            </td>
                                                            <td class="text-center">
                                                                <?= $item->precio ?>
                                                            </td>
                                                            <td class="text-center">
                                                                <?= get_estado($item->estado) ?>
                                                            </td>
                                                            <td class="center">
                                                                <a class="delete deleteE" data-id="<?= $item->idElemento ?>">
                                                                    <img src="<?= base_url('assets/img/delete.svg') ?>" alt="">
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    <?php
                                                    endforeach;
                                                    ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <a href="<?= base_url('manager/analisis') ?>" class="btn btn-primary pull-right">Regresar a Listado</a>
                                    <?php
                                    echo form_close();
                                    ?>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <!-- /MAIN -->