 <?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

    <?php $this->view('layouts/back/mensajes'); ?>

    <!-- MAIN -->
        <main>
            <div class="container">
                <div class="col-md-12 tabs">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">
                            <ul class="nav nav-tabs nav-justified">
                                <li class="active">
                                    <a href="#servicio" aria-controls="servicio" role="tab" data-toggle="tab">Análisis</a>
                                </li>
                            </ul>
                        </div>
                        <hr style="width: 100%; display: block; float: left; margin-top: 0;">
                        <div class="col-md-12 view-item">
                            <div class="tab-content">

                                <div role="tabpanel" class="tab-pane fade in active" id="servicio">
                                    <input type="hidden" name="urlDeleteAnalisis" id="urlDeleteAnalisis" value="<?= base_url('dashboard/adelete/') ?>">
                                    <div class="table-responsive">
                                        <table class="table" data-page-length='10'>
                                            <caption>
                                                ANÁLISIS
                                                <a href="<?= base_url('manager/analisis/create') ?>" class="btn btn-create pull-right">Nuevo Ánalisis</a>
                                            </caption>
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach ($lst as $item):
                                            ?>
                                                <tr class="alert" id="analisis_<?= $item->idAnalisis ?>">
                                                    <td>
                                                        <label>Cód.</label>
                                                        <?= $item->idAnalisis ?>
                                                    </td>
                                                    <td>
                                                        <label>Análisis</label>
                                                        <?= $item->nombre ?>
                                                    </td>
                                                    <td>
                                                        <label>Servicio</label>
                                                        <?= get_servicio($item->idServicio) ?>
                                                    </td>
                                                    <td>
                                                        <label>Grupo Ánalisis</label>
                                                        <?= get_grupo($item->idGrupo_Analisis) ?>
                                                    </td>
                                                    <td>
                                                        <label>Tipo</label>
                                                        <?= get_tipo($item->tipo) ?>
                                                    </td>
                                                    <td>
                                                        <label>Estado</label>
                                                        <?= get_estado($item->estado) ?>
                                                    </td>
                                                    <td class="center">
                                                        <label>Editar</label>
                                                        <a href="<?= base_url('manager/analisis/editar/'.$item->idAnalisis) ?>">
                                                            <img src="../assets/img/edit.svg" alt="">
                                                        </a>
                                                    </td>
                                                    <td class="center">
                                                        <label>Eliminar</label>
                                                        <a class="delete deleteA" data-id="<?= $item->idAnalisis ?>">
                                                            <img src="../assets/img/delete.svg" alt="">
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?php
                                            endforeach;
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <!-- /MAIN -->