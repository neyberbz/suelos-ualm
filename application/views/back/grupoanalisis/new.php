 <?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

        <!-- MAIN -->
        <main>
            <div class="container">
                <div class="col-md-12 tabs">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">
                            <ul class="nav nav-tabs nav-justified">
                                <li class="active">
                                    <a href="#servicio" aria-controls="servicio" role="tab" data-toggle="tab">
                                        <?= $seccion_title ?>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <hr style="width: 100%; display: block; float: left; margin-top: 0;">
                        <div class="col-md-12 view-item">
                            <div class="tab-content">

                                <div role="tabpanel" class="tab-pane fade in active" id="servicio">

                                    <?php
                                    $attributes = array('role' => 'form');
                                    echo form_open_multipart('dashboard/gnuevo', $attributes);
                                    ?>

                                        <div class="form-group required">
                                            <label for="nombre">Nombre</label>
                                            <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Ingresar nombre" required="required">
                                        </div>
                                        <div class="form-group required">
                                            <label for="descripcion">Descripción</label>
                                            <textarea name="descripcion" class="form-control" rows="4" placeholder="Ingresar descripción" required="required"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="estado">Estado</label>
                                            <div data-toggle="buttons">
                                                <?= get_estado_form(3) ?>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary pull-left">Crear Grupo de Ánalisis</button>
                                        <a href="<?= base_url('manager/grupo_analisis') ?>" class="btn btn-primary pull-right">Regresar a Listado</a>
                                    <?php
                                    echo form_close();
                                    ?>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <!-- /MAIN -->