<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h1>Se registrado correctamente</h1>
                <a class="btn" href="<?=base_url('user/register');?>">Regresar a listado</a>
			</div>
		</div>
	</div><!-- .row -->
</div><!-- .container -->