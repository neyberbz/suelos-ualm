<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="layout">
	<div class="container-fluid">
		<div class="row">

			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="page-header col-md-12 col-sm-12 col-xs-12">
					<h1>Registro de Usuarios</h1>
				</div>
			</div>

			<div class="col-md-6 col-sm-6 col-xs-12">

				<?php if (validation_errors()) : ?>
					<div class="col-md-12">
						<div class="alert alert-danger" role="alert">
							<?= validation_errors() ?>
						</div>
					</div>
				<?php endif; ?>
				<?php if (isset($error)) : ?>
					<div class="col-md-12">
						<div class="alert alert-danger" role="alert">
							<?= $error ?>
						</div>
					</div>
				<?php endif; ?>
				<div class="col-md-12">
					<?= form_open() ?>
						<div class="form-group">
							<label for="username">Usuario</label>
							<input type="text" class="form-control" id="username" name="username" placeholder="Usuario">
							<p class="help-block">Al menos 4 caracteres, letras o números solamente</p>
						</div>
						<div class="form-group">
							<label for="email">Email</label>
							<input type="email" class="form-control" id="email" name="email" placeholder="Enter your email">
							<p class="help-block">Una dirección de correo electrónico válida</p>
						</div>
						<div class="form-group">
							<label for="password">Contraseña</label>
							<input type="password" class="form-control" id="password" name="password" placeholder="Enter a password">
							<p class="help-block">Al menos 6 caracteres</p>
						</div>
						<div class="form-group">
							<label for="password_confirm">Repetir contraseña</label>
							<input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Confirm your password">
							<p class="help-block">Debe coincidir con la contraseña</p>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-default">Registrar nuevo usuario</button>
						</div>
					</form>
				</div>

			</div>

			<div class="col-md-6 col-sm-6 col-xs-12">
				<input type="hidden" id="urlDel" value="<?php echo base_url('user/delete/'); ?>">
                <input type="hidden" id="tipoController" value="usuario">
                <div class="table-responsive">
                    <table class="table table-striped table-hover" data-page-length='10'>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Descripción</th>
                                <th data-orderable="false">Controles</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            foreach ($lst_user as $user):
                            	if($user->is_admin == 0):
                        ?>
                            <tr id="cat-<?php echo $user->id; ?>">
                                <td><?php echo $user->id; ?></td>
                                <td><?php echo $user->username; ?></td>
                                <td><?php echo $user->email; ?></td>
                                <td>
                                    <a href="javascript:del(<?php echo $user->id; ?>)" class="btn btn-raised btn-danger btn-xs">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </a>
                                </td>
                            </tr>
                        <?php
                        		endif;
                            endforeach;
                        ?>
                        </tbody>
                    </table>
                </div>
			</div>

		</div><!-- .row -->
	</div><!-- .container -->
</div>