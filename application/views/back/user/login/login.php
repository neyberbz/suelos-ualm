        <div class="bg">
            <div class="container">
                <div class="col-md-6 col-md-offset-3 login">
                    <header class="col-md-12">
                        <a href="#"><img src="<?= base_url() ?>/assets/img/logo.jpg" class="img-responsive img-center" alt=""></a>
                    </header>
                    <main class="col-md-12">
                        <?= form_open() ?>
                        <form action="panel-principal.html" method="post">
                            <h2 class="text-center">BIENVENIDO</h2>
                            <h3 class="text-center">ingresa a nuestro portal de la web</h3>
                            <?php if (validation_errors()) : ?>
                                <div class="col-md-12">
                                    <div class="alert alert-danger" role="alert">
                                    <?= validation_errors() ?>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <?php if (isset($error)) : ?>
                                <div class="alert alert-danger" role="alert">
                                    <?= $error ?>
                                </div>
                            <?php endif; ?>
                            <div class="col-md-10 col-md-offset-1">
                                <div class="form-group">
                                    <input type="text" name="username" placeholder="Correo electrónico" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" placeholder="Ingresar contraseña" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn form-control">LOG IN</button>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"> Recordar
                                    </label>
                                </div>
                            </div>
                        </form>
                    </main>
                </div>
            </div>
        </div>