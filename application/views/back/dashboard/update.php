        <div class="panel panel-default">

            <div class="panel-heading">
                <div class="panel-title">
                    Actualizar Redes Sociales
                </div>
            </div>

            <div class="panel-body">

                <?php if (validation_errors()) : ?>
                <div class="alert alert-danger" role="alert">
                    <?= validation_errors() ?>
                </div>
                <?php endif; ?>

                <?php
                foreach ($redes as $item):
                ?>

                <?php
                $attributes = array('role' => 'form');
                echo form_open('manager/master/update', $attributes);
                ?>

                    <input type="hidden" name="id" value="<?= $item->id ?> required">
                    <input type="hidden" name="key" value="redes" required>
                    <input type="hidden" name="nombre" value="<?= $item->nombre ?>" required>

                    <div class="form-group form-group-default required ">
                        <label><?= $item->nombre ?></label>
                        <input type="text" class="form-control" name="descripcion" placeholder="ej: http://enlaceredsoacial.com/userid" required="required" value="<?= $item->descripcion ?>">
                    </div>

                    <button type="submit" class="btn btn-primary">
                        <span>Actualizar <?= $item->nombre ?></span>
                    </button>

                <?php
                echo form_close();
                ?>

                <?php
                endforeach;
                ?>

            </div>

        </div>