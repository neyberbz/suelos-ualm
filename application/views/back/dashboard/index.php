 <?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

    <?php // $this->view('layouts/back/mensajes'); ?>

    <!-- MAIN -->
        <main>
            <div class="container">
                <div class="col-md-12 tabs">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">
                            <ul class="nav nav-tabs nav-justified">
                                <li class="active">
                                    <a href="#cotizaciones" aria-controls="cotizaciones" role="tab" data-toggle="tab">cotizaciones</a>
                                </li>
                                <li>
                                    <a href="#estadisticas" aria-controls="estadisticas" role="tab" data-toggle="tab">Estadísticas</a>
                                </li>
                            </ul>
                        </div>
                        <hr style="width: 100%; display: block; float: left; margin-top: 0;">
                        <div class="col-md-12 view-item">
                            <div class="tab-content">

                                <div role="tabpanel" class="tab-pane fade in active" id="cotizaciones">
                                    <div class="table-responsive">
                                        <table class="table" data-page-length='5'>
                                            <caption>COTIZACIONES</caption>
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <!-- <tr class="alert" id="item2">
                                                    <td>
                                                        <label>Cliente</label>
                                                        Martha Belaunde
                                                    </td>
                                                    <td>
                                                        <label>Teléfono</label>
                                                        5756454157
                                                    </td>
                                                    <td>
                                                        <label>Correo electrónico</label>
                                                        marthabelaunde@gmail.com
                                                    </td>
                                                    <td>
                                                        <label>Nº de Cotización</label>
                                                        N° 165
                                                    </td>
                                                    <td class="center">
                                                        <label>Total</label>
                                                        <span>S/120.00</span>
                                                    </td>
                                                    <td class="center">
                                                        <label>Editar</label>
                                                        <a href="editar.html"><img src="../assets/img/edit.svg" alt=""></a>
                                                    </td>
                                                    <td class="center">
                                                        <label>Eliminar</label>
                                                        <a class="delete" onclick="delet_item('#item2');"><img src="../assets/img/delete.svg" alt=""></a>
                                                    </td>
                                                    <td class="center"><a href="ver-detalles.html" class="btn" title="">ver detalles</a></td>
                                                </tr> -->
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane fade" id="estadisticas">
                                    <h5>ESTADÍSTICAS</h5>
                                    <hr>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6 row">
                                                <div class="row">
                                                    <div class='col-md-6 dp'>
                                                        <div class="row">
                                                            <label class="col-md-5">Fecha Inicial:</label>
                                                            <div class="form-group col-md-7">
                                                                <div class='input-group date' id='datetimepicker1'>
                                                                    <input type='text' class="form-control" />
                                                                    <span class="input-group-addon">
                                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-6 dp'>
                                                        <div class="row">
                                                            <label class="col-md-5">Fecha Final:</label>
                                                            <div class="form-group col-md-7">
                                                                <div class='input-group date' id='datetimepicker2'>
                                                                    <input type='text' class="form-control" />
                                                                    <span class="input-group-addon">
                                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <form action="" class="form-inline">
                                                <div class="col-md-6 dp">
                                                    <div class="row">
                                                        <label>Registro del pedido de: </label>
                                                        <input type='text' class="form-control" /> <label>Al</label>
                                                        <input type='text' class="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <a href="#" class="btn pull-right download">descargar</a>
                                                        <a href="#" class="btn pull-right print">IMPRIMIR</a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 tbl tbl-edit">
                                        <div class="row">
                                            <div class="table-responsive tbEst">
                                                <table class="table tableEst" data-page-length='5'>
                                                    <thead>
                                                        <tr>
                                                            <th>Nombre del Servicio</th>
                                                            <th>Fecha</th>
                                                            <th>Nº de Pedidos</th>
                                                            <th>Precio Total</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <!-- <tr>
                                                            <td>Parámetros Hídricos: CC, PM, Densidad aparente, Textura</td>
                                                            <td>27/09/2016</td>
                                                            <td><input type="text" class="form-control cant" value="30" readonly="readonly" onkeypress="return justNumbers(event);"></td>
                                                            <td><input type="text" class="form-control total" value="S/. 100.00" readonly="readonly"></td>
                                                        </tr> -->
                                                    </tbody>
                                                </table>
                                                <div class="total total1">
                                                    <div class="col-md-9"></div>
                                                    <div class="col-md-1">total</div>
                                                    <div class="col-md-1">300</div>
                                                    <div class="col-md-1">S/. 154.00</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <!-- /MAIN -->