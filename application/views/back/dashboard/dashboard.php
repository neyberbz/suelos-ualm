<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
        <!-- START PAGE-CONTAINER -->
        <div class="page-container ">

            <?php $this->view('layouts/back/nav'); ?>

            <!-- START PAGE CONTENT WRAPPER -->
            <div class="page-content-wrapper ">

                <!-- START PAGE CONTENT -->
                <div class="content">

                    <!-- START CONTAINER FLUID -->
                    <div class="container-fluid">

                        <div class="row">

                            <!-- BEGIN PlACE PAGE CONTENT HERE -->
                            <?php
                            if(isset($template)):
                                $this->view($template);
                            endif;
                            ?>
                            <!-- END PLACE PAGE CONTENT HERE -->

                        </div>

                    </div>
                    <!-- END CONTAINER FLUID -->

                </div>
                <!-- END PAGE CONTENT -->

            </div>
            <!-- END PAGE CONTENT WRAPPER -->
        </div>
        <!-- END PAGE CONTAINER -->