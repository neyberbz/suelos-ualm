        <!-- TENOR PAGINAS INTERNAS -->
        <div class="container-fluid" id="google-map"></div>
        <!-- /TENOR PAGINAS INTERNAS -->

        <!-- MAIN -->
        <main>
            <!-- NOSOTROS -->
            <div class="container">
                <section class="col-xs-12 col-sm-12 col-md-12 pad_left">
                    <div class="row section">
                        <article>
                            <div class="carrito">
                                <footer>
                                    <div class="col-md-12 col-sm-12 col-xs-12 contacto">
                                        <h2>Contáctenos</h2>
                                        <p>Llena el formulario y nos pondremos en contacto contigo</p>
                                        <h3>FORMULARIO</h3>
                                        <form action="" method="post" id="form">
                                            <div class="inputs">
                                                <div class="col-md-3">
                                                    Nombre
                                                    <input type="text" name="nombre" id="nombre" class="form-control nombre" required>
                                                </div>
                                                <div class="col-md-3">
                                                    Apellidos
                                                    <input type="text" name="apellido" id="apellido" class="form-control apellido" required>
                                                </div>
                                                <div class="col-md-3">
                                                    Teléfonos
                                                    <input type="text" name="tlf" id="tlf" class="form-control tlf" required>
                                                </div>
                                                <div class="col-md-3">
                                                    Correo eléctronico
                                                    <input type="text" name="email" id="email" class="form-control email" required>
                                                </div>
                                                <div class="col-md-12">
                                                    Mensaje
                                                    <textarea name="asunto" id="asunto" class="form-control asunto" rows="4" required></textarea>
                                                </div>
                                            </div>
                                            <!-- <button type="button" class="btn send pull-left" data-toggle="modal" data-target="#gracias">ENVIAR</button> -->
                                            <button type="submit" class="btn send pull-left">ENVIAR</button>
                                        </form>
                                        <h3>DIRECCIÓN</h3>
                                        <address>
                                            <p><img src="assets/img/map.svg" class="img-responsive"><?= $contactenos[0]['direccion'] ?></p>
                                            <p><img src="assets/img/phone.svg" class="img-responsive"><?= $contactenos[0]['telefono'] ?></p>
                                            <p><img src="assets/img/phone.svg" class="img-responsive"><?= $contactenos[0]['telefax'] ?></p>
                                            <p><img src="assets/img/mail.svg" class="img-responsive"><?= $contactenos[0]['email'] ?></p>
                                            <p><img src="assets/img/correo.svg" class="img-responsive"><?= $contactenos[0]['horario'] ?></p>
                                        </address>
                                    </div>
                                </footer>
                            </div>
                        </article>

                    </div>
                </section>
            </div>
            <!-- /NOSOTROS -->
        </main>
        <!-- /MAIN -->