        <!-- TENOR PAGINAS INTERNAS -->
        <div class="container-fluid" id="tenor-page">
            <img src="<?= $faq[0]['img_tenor'] ?>" class="img-responsive">
            <div class="tenor">
                <div class="text"><?= $faq[0]['titulo'] ?></div>
            </div>
        </div>
        <!-- /TENOR PAGINAS INTERNAS -->

        <!-- MAIN -->
        <main class="mrgBot">
            <!-- SERVICIOS -->
            <div class="container">
                <aside class="col-xs-12 col-sm-4 col-md-4 sidebar">
                    <!-- SUBMENU -->
                    <div class="row">
                        <ul class="list-unstyled submenu">
                            <li class="active"><a>preguntas frecuentes</a></li>
                        </ul>
                    </div>
                    <!-- /SUBMENU -->
                </aside>
                <section class="col-xs-12 col-sm-8 col-md-8">
                    <div class="row section">
                        <div class="panel-group" id="accordion" role="tablist">
                        <?php
                        foreach ($master as $item):
                        ?>
                            <div class="panel panel-default">
                                <div class="panel-heading" id="heading<?= $item->id ?>">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="icon" class="open" href="#collapse<?= $item->id ?>">
                                        <?= $item->title->rendered ?>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse<?= $item->id ?>" class="content-tab">
                                    <div class="panel-body">
                                        <?= $item->content->rendered ?>
                                    </div>
                                </div>
                            </div>
                        <?php
                        endforeach;
                        ?>
                           <!--  <div class="panel panel-default">
                                <div class="panel-heading" id="headingOne">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="icon" class="open" href="#collapseOne">
                                        1. EN CUANTO TIEMPO SE TIENEN LOS RESULTADOS?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="content-tab">
                                    <div class="panel-body">
                                        <p>Lorem ipsum es el texto que se usa habitualmente en diseño gráfico en demostraciones de tipografías o de borradores de diseño para probar el diseño visual antes de insertar el texto final.</p>
                                        <p>Aunque no posee actualmente fuentes para justificar sus hipótesis, el profesor de filología clásica Richard McClintock asegura que su uso se remonta a los impresores de comienzos del siglo XVI.1 Su uso en algunos editores de texto muy conocidos en la actualidad ha dado al texto lorem ipsum nueva popularidad.</p>
                                        <p>El texto en sí no tiene sentido, aunque no es completamente aleatorio, sino que deriva de un texto de Cicerón en lengua latina, a cuyas palabras se les han eliminado sílabas o letras. El significado del texto no tiene importancia, ya que solo es una demostración o prueba, pero se inspira en la obra de Cicerón De finibus bonorum et malorum (Sobre los límites del bien y del mal) que comienza con</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="icon" href="#collapseTwo">
                                        2. EN CUANTO TIEMPO SE TIENEN LOS RESULTADOS?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="content-tab">
                                    <div class="panel-body">
                                        <p>Lorem ipsum es el texto que se usa habitualmente en diseño gráfico en demostraciones de tipografías o de borradores de diseño para probar el diseño visual antes de insertar el texto final.</p>
                                        <p>Aunque no posee actualmente fuentes para justificar sus hipótesis, el profesor de filología clásica Richard McClintock asegura que su uso se remonta a los impresores de comienzos del siglo XVI.1 Su uso en algunos editores de texto muy conocidos en la actualidad ha dado al texto lorem ipsum nueva popularidad.</p>
                                        <p>El texto en sí no tiene sentido, aunque no es completamente aleatorio, sino que deriva de un texto de Cicerón en lengua latina, a cuyas palabras se les han eliminado sílabas o letras. El significado del texto no tiene importancia, ya que solo es una demostración o prueba, pero se inspira en la obra de Cicerón De finibus bonorum et malorum (Sobre los límites del bien y del mal) que comienza con</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="icon" href="#collapseThree">
                                        3. EN CUANTO TIEMPO SE TIENEN LOS RESULTADOS?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="content-tab">
                                    <div class="panel-body">
                                        <p>Lorem ipsum es el texto que se usa habitualmente en diseño gráfico en demostraciones de tipografías o de borradores de diseño para probar el diseño visual antes de insertar el texto final.</p>
                                        <p>Aunque no posee actualmente fuentes para justificar sus hipótesis, el profesor de filología clásica Richard McClintock asegura que su uso se remonta a los impresores de comienzos del siglo XVI.1 Su uso en algunos editores de texto muy conocidos en la actualidad ha dado al texto lorem ipsum nueva popularidad.</p>
                                        <p>El texto en sí no tiene sentido, aunque no es completamente aleatorio, sino que deriva de un texto de Cicerón en lengua latina, a cuyas palabras se les han eliminado sílabas o letras. El significado del texto no tiene importancia, ya que solo es una demostración o prueba, pero se inspira en la obra de Cicerón De finibus bonorum et malorum (Sobre los límites del bien y del mal) que comienza con</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingFourth">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="icon" href="#collapseFourth">
                                        4. EN CUANTO TIEMPO SE TIENEN LOS RESULTADOS?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseFourth" class="content-tab">
                                    <div class="panel-body">
                                        <p>Lorem ipsum es el texto que se usa habitualmente en diseño gráfico en demostraciones de tipografías o de borradores de diseño para probar el diseño visual antes de insertar el texto final.</p>
                                        <p>Aunque no posee actualmente fuentes para justificar sus hipótesis, el profesor de filología clásica Richard McClintock asegura que su uso se remonta a los impresores de comienzos del siglo XVI.1 Su uso en algunos editores de texto muy conocidos en la actualidad ha dado al texto lorem ipsum nueva popularidad.</p>
                                        <p>El texto en sí no tiene sentido, aunque no es completamente aleatorio, sino que deriva de un texto de Cicerón en lengua latina, a cuyas palabras se les han eliminado sílabas o letras. El significado del texto no tiene importancia, ya que solo es una demostración o prueba, pero se inspira en la obra de Cicerón De finibus bonorum et malorum (Sobre los límites del bien y del mal) que comienza con</p>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </section>
            </div>
            <!-- /SERVICIOS -->
        </main>
        <!-- /MAIN -->