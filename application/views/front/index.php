
        <main>
            <!-- SERVICIOS -->
            <div class="container" id="servicios">
                <div class="row">
                    <h2 class="text-center">SERVICIOS</h2>
                    <h5 class="text-center">especialistas a tu disposición</h5>
                </div>
                <div class="row">
                    <!-- item-servicio -->
                    <?php
                    foreach ($lst_servicios as $item):
                    ?>
                    <div class="col-xs-12 col-sm-4 col-md-4 servicio">
                        <a href="<?= base_url('servicios') ?>/<?= $item->slug ?>">
                            <img src="<?= base_url('uploads/servicios/'.$item->idServicio.'/'.get_img_servicio($item->idImagen)) ?>" class="img-responsive img-center">
                            <span class="link">
                                <?php echo strtoupper($item->nombre); ?>
                                <img src="assets/img/arrow-right.svg" class="img-responsive pull-right">
                            </span>
                        </a>
                    </div>
                    <?php
                    endforeach;
                    ?>
                    <!-- /item-servicio -->
                </div>
            </div>
            <!-- /SERVICIOS -->
            <!-- COTIZADOR -->
            <div class="container-fluid" id="cotizador">
                <div class="container">
                    <div class="row">
                        <h2 class="text-center">COTIZADOR</h2>
                        <h5 class="text-center">tienes alguna muestra que desees analizar?</h5>
                    </div>
                    <div class="item-frm titulos">
                        <div class="col-xs-12 col-sm-12 col-md-8">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    <div class="row">Que servicio es?</div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    <div class="row">Para que tipo de uso</div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    <div class="row">Detalle</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4 inputs-text">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="row">Cantidad</div>
                                </div>
                                <div class="col-md-4">
                                    <div class="row">Precio Unitario</div>
                                </div>
                                <div class="col-md-4">
                                    <div class="row">Precio Total</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form action="carrito.html" name="cotizador" id="cotiz" method="post" accept-charset="utf-8">
                        <input type="hidden" name="total_items" id="total_items" value="2">
                        <div class="carrito" id="content-item-frm">
                            <div class="item-frm" id="item-1">
                                <div class="col-xs-12 col-sm-12 col-md-8">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                            <div class="row select">
                                                <select class="form-control" required>
                                                    <option value="1">Analisis de suelo</option>
                                                    <option value="2">Analisis de suelo</option>
                                                    <option value="3">Analisis de suelo</option>
                                                    <option value="4">Analisis de suelo</option>
                                                    <option value="5">Analisis de suelo</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                            <div class="row select">
                                                <select class="form-control" required>
                                                    <option>Para riego</option>
                                                    <option>Para riego</option>
                                                    <option>Para riego</option>
                                                    <option>Para riego</option>
                                                    <option>Para riego</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                            <div class="row select">
                                                <select class="form-control" required>
                                                    <option>Ph</option>
                                                    <option>Ph</option>
                                                    <option>Ph</option>
                                                    <option>Ph</option>
                                                    <option>Ph</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4 inputs-text">
                                    <div class="row item_costos">
                                        <input type="text" class="form-control col-md-4 item_cantidad" name="" value="1" onkeypress="return justNumbers(event);" onchange="itemTotal(event, 1);" required>
                                        <input type="text" class="form-control col-md-4 item_precio" data-precio="50" name="" value="S/ 50" readonly>
                                        <input type="text" class="form-control col-md-4 item_total" data-total="50" name="" value="S/ 50" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="item-frm" id="item-2">
                                <div class="col-xs-12 col-sm-12 col-md-8">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                            <div class="row select">
                                                <select class="form-control" required>
                                                    <option value="1">Analisis de suelo</option>
                                                    <option value="2">Analisis de suelo</option>
                                                    <option value="3">Analisis de suelo</option>
                                                    <option value="4">Analisis de suelo</option>
                                                    <option value="5">Analisis de suelo</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                            <div class="row select">
                                                <select class="form-control" required>
                                                    <option>Para riego</option>
                                                    <option>Para riego</option>
                                                    <option>Para riego</option>
                                                    <option>Para riego</option>
                                                    <option>Para riego</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                            <div class="row select">
                                                <select class="form-control" required>
                                                    <option>Ph</option>
                                                    <option>Ph</option>
                                                    <option>Ph</option>
                                                    <option>Ph</option>
                                                    <option>Ph</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4 inputs-text">
                                    <div class="row item_costos">
                                        <input type="text" class="form-control col-md-4 item_cantidad" name="" value="1" onkeypress="return justNumbers(event);" onchange="itemTotal(event, 2);" required>
                                        <input type="text" class="form-control col-md-4 item_precio" data-precio="50" name="" value="S/ 50" readonly>
                                        <input type="text" class="form-control col-md-4 item_total" data-total="50" name="" value="S/ 50" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row footer">
                            <div class="col-xs-12 col-sm-6 col-md-6 text-left">
                                <a class="plus-item-frm">
                                    <img src="assets/img/mas.svg" width="16px">
                                    Deseas agregar otra muestra?
                                </a>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 text-right total">
                                <span>Total: S/</span>
                                <span id="sub_total">100</span>
                            </div>
                        </div>
                        <hr>
                        <div class="row text-center">
                            <!-- <a href="carrito-a.html" class="btn">FINALIZAR COTIZACIÓN</a> -->
                            <button type="submit" class="btn">FINALIZAR COTIZACIÓN</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /COTIZADOR -->

            <!-- TESTIMONIOS -->
            <div class="container" id="testimonios">
                <div class="row">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                        <?php
                        $x = 0;
                        foreach ($lst_testimonios as $item):
                            if($x == 0): $active = 'active'; else: $active = ''; endif;
                        ?>
                            <li data-target="#carousel-example-generic" data-slide-to="<?= $x ?>" class="<?= $active ?>"></li>
                        <?php
                            $x++;
                        endforeach;
                        ?>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <?php
                            $x = 0;
                            foreach ($lst_testimonios as $item):
                                if($x == 0): $active = 'active'; else: $active = ''; endif;
                            ?>
                            <div class="item <?= $active ?>">
                                <div class="carousel-caption">
                                    <?php echo $item['testimonio']; ?>
                                    <h3><?php echo $item['titulo']; ?></h3>
                                    <h4><?php echo $item['subtitulo']; ?></h4>
                                </div>
                            </div>
                            <?php
                                $x++;
                            endforeach;
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /TESTIMONIOS -->
        </main>
        <!-- /MAIN -->