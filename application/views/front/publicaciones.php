        <!-- TENOR PAGINAS INTERNAS -->
        <div class="container-fluid" id="tenor-page">
            <img src="<?= $master[0]['img_tenor'] ?>" class="img-responsive">
            <div class="tenor">
                <div class="text"><?= $master[0]['titulo'] ?></div>
            </div>
        </div>
        <!-- /TENOR PAGINAS INTERNAS -->

        <!-- MAIN -->
        <main>
            <!-- NOSOTROS -->
            <div class="container">
                <aside class="col-xs-12 col-sm-4 col-md-4 sidebar sidebar_qs">

                    <!-- SUBMENU -->
                    <div class="row">
                        <h3 class="titulo uppercase"><?= $master[0]['titulo'] ?></h3>
                    </div>
                    <!-- /SUBMENU -->

                    <!-- SLIDER-SERVICIOS -->
                    <?php $this->load->view('layouts/front/slider_servicios_aside'); ?>
                    <!-- /SLIDER-SERVICIOS -->

                </aside>

                <section class="col-xs-12 col-sm-8 col-md-8">
                    <ol class="breadcrumb">
                        <li><a href="<?= base_url() ?>">Homepage</a></li>
                        <li class="active capitalize"><?= $master[0]['titulo'] ?></li>
                    </ol>
                    <div class="row section">
                        <article>
                            <header><h1><?= $master[0]['titulo'] ?></h1></header>
                            <div class="texto_contenedor">
                                <?= $master[0]['contenido'] ?>
                            </div>
                        </article>

                    </div>
                </section>
            </div>
            <!-- /NOSOTROS -->
        </main>
        <!-- /MAIN -->