        <!-- TENOR PAGINAS INTERNAS -->
        <div class="container-fluid" id="tenor-page">
            <img src="<?= $servicio[0]['img_tenor'] ?>" class="img-responsive">
            <div class="tenor">
                <div class="text"><?= $servicio[0]['titulo'] ?></div>
            </div>
        </div>
        <!-- /TENOR PAGINAS INTERNAS -->

        <!-- MAIN -->
        <main>
            <!-- NOSOTROS -->
            <div class="container mrgBot">
                <aside class="col-xs-12 col-sm-4 col-md-4 sidebar sidebar_qs">
                    <!-- SUBMENU -->
                    <div class="row">
                        <h3 class="titulo">SERVICIOS</h3>
                        <ul class="list-unstyled submenu submenu_nosotros">
                        <?php
                        foreach ($submenu as $item):
                        ?>
                            <li><a class="capitalize" href="<?= base_url('servicios/'.$item['slug']) ?>"><?= $item['titulo'] ?></a></li>
                        <?php
                        endforeach;
                        ?>
                        </ul>
                    </div>
                    <!-- /SUBMENU -->
                    <!-- BANNER -->
                    <div class="row hidden-xs">
                        <div class="banner">
                            <img src="<?= base_url('assets') ?>/img/banner_servicios.jpg" class="img-center img-responsive" alt="">
                            <div class="tenor">
                                <div class="txt">
                                    Deseas cotizar<br>
                                    este servicio?<br>
                                    <button class="btn"><span>COTIZAR</span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /BANNER -->
                    <!-- SLIDER-SERVICIOS -->
                    <div class="row hidden-xs">
                        <!-- Carousel -->
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <header>Tambien te podria interezar</header>
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img src="<?= base_url('assets') ?>/img/slider-servicios.jpg" alt="" class="img-responsive img-center">
                                    <!-- Static Header -->
                                    <div class="tenor">
                                        <div class="col-md-12 text-center">
                                            <h2>Analisis de agua<span>con fines de riego</span></h2>
                                            <button class="btn" type=""><span>ver servicio</span></button>
                                        </div>
                                    </div><!-- /header-text -->
                                </div>
                                <div class="item">
                                    <img src="<?= base_url('assets') ?>/img/slider-servicios.jpg" alt="" class="img-responsive img-center">
                                    <!-- Static Header -->
                                    <div class="tenor">
                                        <div class="col-md-12 text-center">
                                            <h2>Analisis de agua<span>con fines de riego</span></h2>
                                            <button class="btn" type=""><span>ver servicio</span></button>
                                        </div>
                                    </div><!-- /header-text -->
                                </div>
                            </div>
                            <!-- Controls -->
                            <footer>
                                <a class="pull-left" href="#carousel-example-generic" data-slide="prev">
                                    <img src="<?= base_url('assets') ?>/img/left_slider_serv.svg">
                                </a>
                                <a class="link" href="servicios.html">VER TODOS LOS SERVICIOS</a>
                                <a class="pull-right" href="#carousel-example-generic" data-slide="next">
                                    <img src="<?= base_url('assets') ?>/img/right_slider_serv.svg">
                                </a>
                            </footer>
                        </div>
                        <!-- /carousel -->
                    </div>
                    <!-- /SLIDER-SERVICIOS -->
                </aside>
                <section class="col-xs-12 col-sm-8 col-md-8 movil">
                    <ol class="breadcrumb">
                        <li><a href="index.html">Homepage</a></li>
                        <li><a href="servicios.html">servicios</a></li>
                        <li class="active">Análisis de Suelos</li>
                    </ol>
                    <div class="row section">
                        <article>
                            <header><h1>Con fines de Agricultura</h1></header>
                            <img src="<?= base_url('assets') ?>/img/servicio.jpg" class="img-responsive img-center">
                            <h2>¿Por qué y cuándo se realizan?</h2>
                            <p>Los análisis de suelos son la parte esencial sobre la que se basa cualquier programa de manejo agronómico en una producción agrícola. Entre los aspectos que vamos a conocer con este tipo de análisis, destacamos.</p>
                            <h3>analisis y costos</h3>
                            <hr>
                            <div class="servicios">
                                <div class="lst-items">
                                    <div class="page pag1 activePage">
                                        <!-- ITEM -->
                                        <div class="item" id="servicio-1">
                                            <h2>ANALISIS Completo de Rutina: pH, CE, MO, P, K, Carbonatos 1</h2>
                                            <div class="arrow_box"></div>
                                            <div class="row col-md-7 pull-left inputs">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        Precio Unitario
                                                        <input type="text" class="form-control unit" name="input_unit" value="20" readonly="readonly">
                                                    </div>
                                                    <div class="col-md-4 text-center">
                                                        Cantidad
                                                        <div class="row select">
                                                            <select class="form-control" onchange="javascript:servicioItem(1);" required>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 text-center">
                                                        Precio Total
                                                        <input type="text" class="form-control total" name="input_total" value="60" readonly="readonly">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row col-md-5 pull-right">
                                                <button class="btn pull-right" data-toggle="modal" data-target="#Modal" data-whatever="@01">AGREGAR</button>
                                            </div>
                                        </div>
                                        <!-- /ITEM -->
                                        <!-- ITEM -->
                                        <div class="item" id="servicio-2">
                                            <h2>ANALISIS Completo de Rutina: pH, CE, MO, P, K, Carbonatos 2</h2>
                                            <div class="arrow_box"></div>
                                            <div class="row col-md-7 pull-left inputs">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        Precio Unitario
                                                        <input type="text" class="form-control unit" name="input_unit" value="20" readonly="readonly">
                                                    </div>
                                                    <div class="col-md-4 text-center">
                                                        Cantidad
                                                        <div class="row select">
                                                            <select class="form-control" onchange="javascript:servicioItem(2);" required>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="10">10</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 text-center">
                                                        Precio Total
                                                        <input type="text" class="form-control total" name="input_total" value="60" readonly="readonly">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row col-md-5 pull-right">
                                                <button class="btn pull-right" data-toggle="modal" data-target="#Modal" data-whatever="@02">AGREGAR</button>
                                            </div>
                                        </div>
                                        <!-- /ITEM -->
                                        <!-- ITEM -->
                                        <div class="item" id="servicio-3">
                                            <h2>ANALISIS Completo de Rutina: pH, CE, MO, P, K, Carbonatos 3</h2>
                                            <div class="arrow_box"></div>
                                            <div class="row col-md-7 pull-left inputs">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        Precio Unitario
                                                        <input type="text" class="form-control unit" name="input_unit" value="20" readonly="readonly">
                                                    </div>
                                                    <div class="col-md-4 text-center">
                                                        Cantidad
                                                        <div class="row select">
                                                            <select class="form-control" onchange="javascript:servicioItem(3);" required>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">...</option>
                                                                <option value="10">10</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 text-center">
                                                        Precio Total
                                                        <input type="text" class="form-control total" name="input_total" value="60" readonly="readonly">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row col-md-5 pull-right">
                                                <button class="btn pull-right" data-toggle="modal" data-target="#Modal" data-whatever="@03">AGREGAR</button>
                                            </div>
                                        </div>
                                        <!-- /ITEM -->
                                        <!-- ITEM -->
                                        <div class="item" id="servicio-4">
                                            <h2>ANALISIS Completo de Rutina: pH, CE, MO, P, K, Carbonatos 4</h2>
                                            <div class="arrow_box"></div>
                                            <div class="row col-md-7 pull-left inputs">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        Precio Unitario
                                                        <input type="text" class="form-control unit" name="input_unit" value="20" readonly="readonly">
                                                    </div>
                                                    <div class="col-md-4 text-center">
                                                        Cantidad
                                                        <div class="row select">
                                                            <select class="form-control" onchange="javascript:servicioItem(4);" required>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="10">10</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 text-center">
                                                        Precio Total
                                                        <input type="text" class="form-control total" name="input_total" value="60" readonly="readonly">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row col-md-5 pull-right">
                                                <button class="btn pull-right" data-toggle="modal" data-target="#Modal" data-whatever="@04">AGREGAR</button>
                                            </div>
                                        </div>
                                        <!-- /ITEM -->
                                        <!-- ITEM -->
                                        <div class="item" id="servicio-5">
                                            <h2>ANALISIS Completo de Rutina: pH, CE, MO, P, K, Carbonatos 5</h2>
                                            <div class="arrow_box"></div>
                                            <div class="row col-md-7 pull-left inputs">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        Precio Unitario
                                                        <input type="text" class="form-control unit" name="input_unit" value="20" readonly="readonly">
                                                    </div>
                                                    <div class="col-md-4 text-center">
                                                        Cantidad
                                                        <div class="row select">
                                                            <select class="form-control" onchange="javascript:servicioItem(5);" required>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="10">10</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 text-center">
                                                        Precio Total
                                                        <input type="text" class="form-control total" name="input_total" value="60" readonly="readonly">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row col-md-5 pull-right">
                                                <button class="btn pull-right" data-toggle="modal" data-target="#Modal" data-whatever="@05">AGREGAR</button>
                                            </div>
                                        </div>
                                        <!-- /ITEM -->
                                    </div>
                                    <div class="page pag2">
                                        <!-- ITEM -->
                                        <div class="item" id="servicio-6">
                                            <h2>ANALISIS Completo de Rutina: pH, CE, MO, P, K, Carbonatos 4</h2>
                                            <div class="arrow_box"></div>
                                            <div class="row col-md-7 pull-left inputs">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        Precio Unitario
                                                        <input type="text" class="form-control unit" name="input_unit" value="20" readonly="readonly">
                                                    </div>
                                                    <div class="col-md-4 text-center">
                                                        Cantidad
                                                        <div class="row select">
                                                            <select class="form-control" onchange="javascript:servicioItem(6);" required>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="10">10</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 text-center">
                                                        Precio Total
                                                        <input type="text" class="form-control total" name="input_total" value="60" readonly="readonly">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row col-md-5 pull-right">
                                                <button class="btn pull-right" data-toggle="modal" data-target="#Modal" data-whatever="@04">AGREGAR</button>
                                            </div>
                                        </div>
                                        <!-- /ITEM -->
                                        <!-- ITEM -->
                                        <div class="item" id="servicio-7">
                                            <h2>ANALISIS Completo de Rutina: pH, CE, MO, P, K, Carbonatos 5</h2>
                                            <div class="arrow_box"></div>
                                            <div class="row col-md-7 pull-left inputs">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        Precio Unitario
                                                        <input type="text" class="form-control unit" name="input_unit" value="20" readonly="readonly">
                                                    </div>
                                                    <div class="col-md-4 text-center">
                                                        Cantidad
                                                        <div class="row select">
                                                            <select class="form-control" onchange="javascript:servicioItem(7);" required>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">7</option>
                                                                <option value="10">10</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 text-center">
                                                        Precio Total
                                                        <input type="text" class="form-control total" name="input_total" value="60" readonly="readonly">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row col-md-5 pull-right">
                                                <button class="btn pull-right" data-toggle="modal" data-target="#Modal" data-whatever="@05">AGREGAR</button>
                                            </div>
                                        </div>
                                        <!-- /ITEM -->
                                    </div>
                                </div>
                                <!-- <nav class="pull-right">
                                    <ul class="pagination">
                                        <li>
                                            <a href="#" aria-label="Previous"><img src="<?= base_url('assets') ?>/img/pag_arrow_left.svg" class="img-responsive img-center"></a>
                                        </li>
                                        <li class="active"><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li>
                                            <a href="#" aria-label="Next"><img src="<?= base_url('assets') ?>/img/pag_arrow_right.svg" class="img-responsive img-center"></a>
                                        </li>
                                    </ul>
                                </nav> -->
                                <nav class="pull-right pages">
                                </nav>
                            </div>
                        </article>

                    </div>
                </section>
                <aside class="col-xs-12 hidden-lg hidden-sm hidden-md sidebar sidebar_qs">
                    <!-- BANNER -->
                    <div class="row">
                        <div class="banner">
                            <img src="<?= base_url('assets') ?>/img/banner_servicios.jpg" class="img-center img-responsive" alt="">
                            <div class="tenor">
                                <div class="txt">
                                    Deseas cotizar<br>
                                    este servicio?<br>
                                    <button class="btn"><span>COTIZAR</span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /BANNER -->
                    <!-- SLIDER-SERVICIOS -->
                    <div class="row">
                        <!-- Carousel -->
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <header>Tambien te podria interezar</header>
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img src="<?= base_url('assets') ?>/img/slider-servicios.jpg" alt="" class="img-responsive img-center">
                                    <!-- Static Header -->
                                    <div class="tenor">
                                        <div class="col-md-12 text-center">
                                            <h2>Analisis de agua<span>con fines de riego</span></h2>
                                            <button class="btn" type=""><span>ver servicio</span></button>
                                        </div>
                                    </div><!-- /header-text -->
                                </div>
                                <div class="item">
                                    <img src="<?= base_url('assets') ?>/img/slider-servicios.jpg" alt="" class="img-responsive img-center">
                                    <!-- Static Header -->
                                    <div class="tenor">
                                        <div class="col-md-12 text-center">
                                            <h2>Analisis de agua<span>con fines de riego</span></h2>
                                            <button class="btn" type=""><span>ver servicio</span></button>
                                        </div>
                                    </div><!-- /header-text -->
                                </div>
                            </div>
                            <!-- Controls -->
                            <footer>
                                <a class="pull-left" href="#carousel-example-generic" data-slide="prev">
                                    <img src="<?= base_url('assets') ?>/img/left_slider_serv.svg">
                                </a>
                                <a class="link" href="servicios.html">VER TODOS LOS SERVICIOS</a>
                                <a class="pull-right" href="#carousel-example-generic" data-slide="next">
                                    <img src="<?= base_url('assets') ?>/img/right_slider_serv.svg">
                                </a>
                            </footer>
                        </div>
                        <!-- /carousel -->
                    </div>
                    <!-- /SLIDER-SERVICIOS -->
                </aside>
            </div>
            <!-- /NOSOTROS -->
        </main>
        <!-- /MAIN -->