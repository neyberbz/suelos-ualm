<!-- TENOR PAGINAS INTERNAS -->
        <div class="container-fluid" id="tenor-page">
            <img src="<?= $post[0]['img_tenor'] ?>" class="img-responsive">
            <div class="tenor">
                <div class="text"><?= $post[0]['titulo'] ?></div>
            </div>
        </div>
        <!-- /TENOR PAGINAS INTERNAS -->

        <!-- MAIN -->
        <main>
            <!-- NOSOTROS -->
            <div class="container">
                <aside class="col-xs-12 col-sm-4 col-md-4 sidebar sidebar_qs">

                    <!-- SUBMENU -->
                    <div class="row">
                        <h3 class="titulo uppercase"><?= $master[0]['titulo'] ?></h3>
                        <ul class="list-unstyled submenu submenu_nosotros">
                            <?php
                            foreach ($submenu as $item):
                            ?>
                            <li class="<?php if($item['slug'] == $slug): echo 'active'; endif; ?>">
                                <a href="<?= base_url('nosotros/'.$item['slug']) ?>" class="uppercase">
                                    <?= $item['titulo'] ?>
                                </a>
                            </li>
                            <?php
                            endforeach;
                            ?>
                        </ul>
                    </div>
                    <!-- /SUBMENU -->

                    <!-- SLIDER-SERVICIOS -->
                    <?php $this->load->view('layouts/front/slider_servicios_aside'); ?>
                    <!-- /SLIDER-SERVICIOS -->

                </aside>

                <section class="col-xs-12 col-sm-8 col-md-8">
                    <ol class="breadcrumb">
                        <li><a href="<?= base_url() ?>">Homepage</a></li>
                        <li><a href="<?= base_url($master[0]['titulo']) ?>" class="capitalize"><?= $master[0]['titulo'] ?></a></li>
                        <li class="active capitalize"><?= $post[0]['titulo'] ?></li>
                    </ol>
                    <div class="row section">
                        <article>
                            <header><h1><?= $post[0]['titulo'] ?></h1></header>
                            <div class="texto_contenedor">
                                <?= $post[0]['contenido'] ?>
                            </div>
                        </article>

                    </div>
                </section>
            </div>
            <!-- /NOSOTROS -->
        </main>
        <!-- /MAIN -->