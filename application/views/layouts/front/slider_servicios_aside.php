                    <div class="row">
                        <!-- Carousel -->
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <header>Tambien te podria interezar</header>
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <?php
                                $x = 0;
                                foreach ($slider_servicios as $item):
                                    if($x == 0): $active = 'active'; else: $active = ''; endif;
                                ?>
                                <div class="item <?= $active ?>">
                                    <img src="<?= $item['img_medium']?>" alt="" class="img-responsive img-center">
                                    <!-- Static Header -->
                                    <div class="tenor">
                                        <div class="col-md-12 text-center">
                                            <h2><?= $item['titulo'] ?></h2>
                                            <a class="btn" href="<?= base_url('servicios/'.$item['slug']) ?>"><span>ver servicio</span></a>
                                        </div>
                                    </div><!-- /header-text -->
                                </div>
                                <?php
                                    $x++;
                                endforeach;
                                ?>
                            </div>
                            <!-- Controls -->
                            <footer>
                                <a class="pull-left" href="#carousel-example-generic" data-slide="prev">
                                    <img src="<?= base_url('assets') ?>/img/left_slider_serv.svg">
                                </a>
                                <a class="link" href="servicios.html">VER TODOS LOS SERVICIOS</a>
                                <a class="pull-right" href="#carousel-example-generic" data-slide="next">
                                    <img src="<?= base_url('assets') ?>/img/right_slider_serv.svg">
                                </a>
                            </footer>
                        </div>
                        <!-- /carousel -->
                    </div>