
        <!-- FOOTER -->
        <footer>
            <div class="container">
                <hr>
                <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2">
                    <div class="col-xs-12 col-sm-12 col-md-4 text-center">
                        <img src="<?= base_url('assets') ?>/img/telefono.svg" class="img-center"><br>
                        <?= $contactenos[0]['telefono'] ?>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 text-center">
                        <img src="<?= base_url('assets') ?>/img/correo.svg" class="img-center"><br>
                        <?= $contactenos[0]['email'] ?>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 text-center">
                        <img src="<?= base_url('assets') ?>/img/map.svg" class="img-center"><br>
                        <?= $contactenos[0]['direccion'] ?>
                    </div>
                </div>
            </div>
            <div class="container">
                <hr>
                <div class="col-xs-12 col-sm-6 col-md-6 text-left">Todos los derechos reservados 2016 © Universidad la Molina</div>
                <div class="col-xs-12 col-sm-6 col-md-6 text-right">Diseño y Desarrollo por La empresa</div>
            </div>
        </footer>
        <!-- /FOOTER -->

        <!-- MODAL -->
        <div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="assets/img/close_modal.svg" class="img-responsive img-center" alt=""></button>
                    <div class="modal-body">
                        <h4 class="text-center">LISTO!</h4>
                        <p class="text-center"><span></span>,<br>que deseas hacer ahora?</p>
                        <div class="col-md-6"><a class="btn pull-right" href="carrito-a.html">FINALIZAR COTIZACION</a></div>
                        <div class="col-md-6"><button class="btn pull-left" data-dismiss="modal" aria-label="Close">VOLVER</button></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /MODAL -->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?= base_url('assets') ?>/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;language=es"></script>
        <script src="<?= base_url('assets') ?>/js/ualm.min.js"></script>

        <script src="<?= base_url('assets') ?>/js/jquery.bootpag.min.js"></script>

        <script src="<?= base_url('assets') ?>/masterslider/jquery.easing.min.js"></script>
        <script src="<?= base_url('assets') ?>/masterslider/masterslider.min.js"></script>

        <script src="<?= base_url('assets') ?>/js/jquery.validate.js"></script>
        <script src="<?= base_url('assets') ?>/js/additional-methods.js"></script>
        <script src="<?= base_url('assets') ?>/js/messages_es_PE.min.js"></script>

        <script type="text/javascript">

            var slider = new MasterSlider();

            slider.control('arrows');
            slider.control('circletimer' , {color:"#FFFFFF" , stroke:9});

            slider.setup('masterslider' , {
               width:1170,
               height:696,
               space:0,
               loop:true,
               view:'fadeWave',
               layout:'partialview'
            });

            $(document).ready(function(){
                $('.pages').bootpag({
                    total: 2,
                    page: 1,
                    maxVisible: 10
                }).on('page', function(event, num){
                    $(".lst-items .page").removeClass("activePage");
                    $(".lst-items .pag"+num).addClass('activePage');
                });
            });

            $.validator.methods.email = function( value, element ) {
                return this.optional( element ) || /([A-Z0-9a-z_-][^@])+?@[^$#<>?]+?\.[\w]{2,4}/.test(value);
            }

            jQuery.validator.addMethod("lettersonly", function(value, element) {
                return this.optional(element) || /^[a-z ]+$/i.test(value);
            }, "Ingresar solo letras");

            $("#form").validate({
                rules: {
                    nombre: {
                        required: true,
                        lettersonly: true,
                        minlength: 2
                    },
                    apellido: {
                        required: true,
                        lettersonly: true,
                        minlength: 2
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    tlf: {
                        required: true,
                        digits: true,
                        minlength: 7
                    },
                    asunto: {
                        required: true,
                        minlength: 2
                    }
                },
                errorElement: "em",
                success: "valid",
                submitHandler: function() { $('#gracias').modal(); }
            });

            $(document).ready(function(){
                function initialize() {
                        var myLatlng = new google.maps.LatLng(<?php echo $contactenos[0]['latlon']; ?>);
                        //var imagePath = 'http://m.schuepfen.ch/icons/helveticons/black/60/Pin-location.png'
                        var imagePath = 'assets/img/map.png'
                        var mapOptions = {
                            zoom: 14,
                            center: myLatlng,
                            mapTypeId: google.maps.MapTypeId.ROADMAP,
                            scrollwheel: false,
                            disableDefaultUI: true
                        }

                    var map = new google.maps.Map(document.getElementById('google-map'), mapOptions);
                    //Callout Content
                    var contentString = 'Some address here..';
                    //Set window width + content
                    var infowindow = new google.maps.InfoWindow({
                        content: contentString,
                        maxWidth: 500
                    });

                    //Add Marker
                    var marker = new google.maps.Marker({
                        position: myLatlng,
                        map: map,
                        icon: imagePath,
                        title: 'image title'
                    });

                    google.maps.event.addListener(marker, 'click', function() {
                        infowindow.open(map,marker);
                    });

                    //Resize Function
                    google.maps.event.addDomListener(window, "resize", function() {
                        var center = map.getCenter();
                        google.maps.event.trigger(map, "resize");
                        map.setCenter(center);
                    });
                }

                google.maps.event.addDomListener(window, 'load', initialize);

            });

        </script>
    </body>
</html>