        <!-- HEADER -->
        <header class="container-fluid">
            <div class="col-xs-12 col-sm-3 col-md-3 brand">
                <a href="<?= base_url() ?>"><img src="<?= base_url('assets') ?>/img/logo.jpg" class="img-responsive img-center brand-logo"></a>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="col-xs-12 col-sm-9 col-md-9">
                <div class="row sub-header">
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <form action="index_submit" method="get" accept-charset="utf-8" class="form-horizontal" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Que estas buscando?" name="q">
                                <div class="input-group-btn">
                                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="hidden-xs col-xs-12 col-sm-6 col-md-6 menu-info">
                        <ul class="list-unstyled nav-justified">
                            <li><img src="<?= base_url('assets') ?>/img/faq.svg" class="pull-left"><a href="<?= base_url('necesitas-ayuda') ?>">Necesitas ayuda?</a></li>
                            <li><img src="<?= base_url('assets') ?>/img/info.svg" class="pull-left"><a href="<?= base_url('carrito') ?>">Información de la Cotización</a></li>
                            <li><img src="<?= base_url('assets') ?>/img/cursor.svg" class="pull-left"><a href="<?= base_url('publicaciones') ?>">Publicaciones</a></li>
                        </ul>
                    </div>
                </div>
                <div id="navbar" class="row menu navbar-collapse collapse">
                    <div class="col-xs-12 col-sm-10 col-md-10">
                        <ul class="list-unstyled nav nav-justified">
                            <li>
                                <a href="<?= base_url('servicios') ?>">
                                    <span class="img img-svg pull-left">
                                        <img src="<?= base_url('assets') ?>/img/servicios.svg" class="active">
                                        <img src="<?= base_url('assets') ?>/img/servicios-hover.svg" class="hover">
                                    </span>
                                    Servicios<br>
                                    <span class="subtitulo">6 Tipos de analisis</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url('nosotros') ?>">
                                    <span class="img pull-left">
                                        <img src="<?= base_url('assets') ?>/img/nosotros.svg" class="active">
                                        <img src="<?= base_url('assets') ?>/img/nosotros-hover.svg" class="hover">
                                    </span>
                                    Nosotros<br>
                                    <span class="subtitulo">100% Especialistas</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url('faq') ?>">
                                    <span class="img pull-left">
                                        <img src="<?= base_url('assets') ?>/img/preguntas.svg" class="active">
                                        <img src="<?= base_url('assets') ?>/img/preguntas-hover.svg" class="hover">
                                    </span>
                                    Preguntas frecuentes<br>
                                    <span class="subtitulo">Tienes alguna consulta?</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url('contactenos') ?>">
                                    <span class="img pull-left">
                                        <img src="<?= base_url('assets') ?>/img/contacto.svg" class="active">
                                        <img src="<?= base_url('assets') ?>/img/contacto-hover.svg" class="hover">
                                    </span>
                                    Contacto<br>
                                    <span class="subtitulo">Estamos para atenderte</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-2 col-md-2">
                        <div class="cotizaciones pull-right">
                            <a href="<?= base_url('carrito') ?>">
                                <span>
                                    <img src="<?= base_url('assets') ?>/img/cotizacion.svg" class="img-center"><br>
                                    5 Cotizaciones
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- /HEADER -->