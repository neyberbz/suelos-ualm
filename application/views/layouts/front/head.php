<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="<?= $page_description ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title><?= $page_title ?></title>

        <!-- Bootstrap -->
        <link href="<?= base_url('assets') ?>/css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?= base_url('assets') ?>/css/ualm.css" rel="stylesheet" />

        <!-- Base MasterSlider style sheet -->
        <link href="<?= base_url('assets') ?>/masterslider/style/masterslider.css" rel="stylesheet"  />
        <!-- Master Slider Skin -->
        <link href="<?= base_url('assets') ?>/masterslider/skins/default/style.css" rel="stylesheet" />

        <script type='text/javascript' src="<?= base_url('assets') ?>/js/modernizr.min.js"></script>
        <script type='text/javascript' src="<?= base_url('assets') ?>/js/css3-mediaqueries.js"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="pagina-interna">