        <button type="button" class="menu">
            <img src="public/img/bar-menu.svg" class="img-responsive img-center" width="28">
        </button>

        <?php if($id == 'es'): ?>
        <ul class="navbar">
            <li><a href="#home">Inicio</a></li>
            <li><a href="#extraccion">Extracción</a></li>
            <li><a href="#produccion">Producción</a></li>
            <li><a href="#productos">Productos</a></li>
            <li><a href="#control">Calidad</a></li>
            <li><a href="#certificaciones">Certificaciones</a></li>
            <li><a href="#contacto">Contacto</a></li>
        </ul>
        <?php else: ?>
        <ul class="navbar">
            <li><a href="#home">Home</a></li>
            <li><a href="#extraccion">Extraction</a></li>
            <li><a href="#produccion">Production</a></li>
            <li><a href="#productos">Products </a></li>
            <li><a href="#control">Quality control</a></li>
            <li><a href="#certificaciones">Certifications</a></li>
            <li><a href="#contacto">Contact us</a></li>
        </ul>
        <?php endif; ?>
