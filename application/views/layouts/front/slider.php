        <!-- SLIDER -->
        <div class="container-fluid" id="slider">
            <!-- template -->
            <div class="ms-partialview-template" id="partial-view-1">
                <!-- masterslider -->
                <div class="master-slider ms-skin-default" id="masterslider">
                <?php
                foreach ($lst_sliders as $item):
                ?>
                    <div class="ms-slide">
                        <img src="assets/masterslider/blank.gif" data-src="<?= $item['img_full'] ?>" alt="<?= $item['titulo'] ?>"/>
                        <div class="ms-info ms-layer" data-type="text" data-effect="rotate3dleft(50,0,0,480)" data-duration="2000" data-ease="easeInOutQuint">
                            <div class="valign">
                                <h3><?= $item['titulo'] ?></h3>
                                <?php echo $item['contenido'] ?>
                                <a href="<?= $item['url'] ?>" class="btn pull-left">Cotizar</a>
                            </div>
                        </div>
                    </div>
                <?php
                endforeach;
                ?>
                </div>
                <!-- end of masterslider -->
            </div>
            <!-- end of template -->
        </div>
        <!-- /SLIDER -->