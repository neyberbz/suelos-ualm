        <!-- HEADER -->
        <?php if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true) : ?>
        <header class="container-fluid">
            <div class="container">
                <div class="col-xs-3 col-md-3 col-md-3 brand">
                    <a href="<?= base_url('manager/dashboard') ?>"><img src="<?= base_url('assets') ?>/img/logo.jpg" class="img-responsive img-center"></a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <form class="col-md-11 col-md-offset-1">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Busca por nombre, numero de codigo....">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <span class="glyphicon glyphicon-search"></span>
                                </button>
                            </span>
                        </div>
                    </form>
                </div>

                <div class="col-xs-3 col-sm-3 col-md-3 log">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <button class="dropdown-toggle pull-right" data-toggle="dropdown">
                                    <img src="<?= base_url('assets') ?>/img/service.svg" class="img-responsive img-center">
                                    <span>Servicios</span>
                                    <img src="<?= base_url('assets') ?>/img/arrow-down.svg" class="img-responsive">
                                </button>
                                <div class="dropdown-servicios dropdown-menu pull-right">
                                    <a href="<?= base_url('manager/servicios') ?>">Servicios</a>
                                    <a href="<?= base_url('manager/analisis') ?>">Análisis</a>
                                    <a href="<?= base_url('manager/grupo_analisis') ?>">Grupo de Ánalisis</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <button class="dropdown-toggle pull-right" data-toggle="dropdown">
                                    <img src="<?= base_url('assets') ?>/img/user.svg" class="img-responsive img-center">
                                    <span><?php echo $_SESSION['username']; ?></span>
                                    <img src="<?= base_url('assets') ?>/img/arrow-down.svg" class="img-responsive">
                                </button>
                                <div class="dropdown-status dropdown-menu pull-right">
                                    <a href="<?= base_url('manager/logout') ?>">cerrar sesión</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    <?php endif; ?>
        <!-- /HEADER -->