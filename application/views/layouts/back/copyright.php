                <!-- START COPYRIGHT -->
                <div class="container-fluid container-fixed-lg footer">
                    <div class="copyright sm-text-center">
                        <p class="small no-margin pull-left sm-pull-reset">
                            <span class="hint-text">Copyright &copy; 2016 </span>
                            <span class="font-montserrat">ODA</span>.
                            <span class="hint-text">Todos los derechos reservados. </span>
                        </p>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <!-- END COPYRIGHT -->