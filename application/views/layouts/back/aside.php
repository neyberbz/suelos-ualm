<!-- BEGIN SIDEBPANEL-->
    <nav class="page-sidebar" data-pages="sidebar">

        <!-- BEGIN SIDEBAR MENU HEADER-->
        <div class="sidebar-header text-center">
            <img src="<?=base_url();?>public/front/img/logo-oda.svg" alt="logo" class="brand" data-src="<?=base_url();?>public/front/img/logo-oda.svg" data-src-retina="<?=base_url();?>public/front/img/logo-oda.svg" width="100">
        </div>
        <!-- END SIDEBAR MENU HEADER-->

        <!-- START SIDEBAR MENU -->
        <div class="sidebar-menu">
            <!-- BEGIN SIDEBAR MENU ITEMS-->
            <ul class="menu-items">

                <li class="m-t-30 <?php echo activate_menu($os = array("index")); ?>">
                    <a href="<?=base_url('dashboard/dashboard');?>" class="detailed">
                        <span class="title">Dashboard</span>
                        <span class="details">Pantalla principal</span>
                    </a>
                    <span class="bg-success icon-thumbnail"><i class="pg-home"></i></span>
                </li>

                <li class="<?php echo activate_menu($os = array("slider", "sedit")); ?>">
                    <a href="<?=base_url('manager/slider')?>" class="detailed">
                        <span class="title">Slider</span>
                        <span class="details">Slider pantalla principal</span>
                    </a>
                    <span class="icon-thumbnail"><i class="pg-layouts3"></i></span>
                </li>

                <li class="<?php echo activate_menu($os = array("nosotros")); ?>">
                    <a href="<?=base_url('manager/nosotros');?>" class="detailed">
                        <span class="title">Quienes Somos</span>
                        <span class="details">Seccion de quienes somos</span>
                    </a>
                    <span class="icon-thumbnail"><i class="pg-layouts3"></i></span>
                </li>

                <li class="<?php echo activate_menu($os = array("productos")); ?>">
                    <a href="<?=base_url('manager/productos');?>" class="detailed">
                        <span class="title">Productos</span>
                        <span class="details">Seccion de categorias y productos</span>
                    </a>
                    <span class="icon-thumbnail"><i class="pg-tables"></i></span>
                </li>

                <li class="<?php echo activate_menu($os = array("procesos")); ?>">
                    <a href="<?=base_url('manager/procesos');?>" class="detailed">
                        <span class="title">Procesos</span>
                        <span class="details">Sección de procesos</span>
                    </a>
                    <span class="icon-thumbnail"><i class="pg-note"></i></span>
                </li>

                <li class="<?php echo activate_menu($os = array("certificaciones")); ?>">
                    <a href="<?=base_url('manager/certificaciones');?>" class="detailed">
                        <span class="title">Certificaciones</span>
                        <span class="details">Sección de certificaciones</span>
                    </a>
                    <span class="icon-thumbnail"><i class="pg-note"></i></span>
                </li>

                <li class="<?php echo activate_menu($os = array("superfood")); ?>">
                    <a href="<?=base_url('manager/superfood');?>" class="detailed">
                        <span class="title">superfood news</span>
                        <span class="details">Sección de certificaciones</span>
                    </a>
                    <span class="icon-thumbnail"><i class="pg-note"></i></span>
                </li>

                <li class="<?php echo activate_menu($os = array("contacto")); ?>">
                    <a href="<?=base_url('manager/contacto');?>" class="detailed">
                        <span class="title">contacto</span>
                        <span class="details">Sección de contacto</span>
                    </a>
                    <span class="icon-thumbnail"><i class="pg-note"></i></span>
                </li>

                <li class="<?php echo activate_menu($os = array("suscritos")); ?>">
                    <a href="<?=base_url('manager/suscritos');?>" class="detailed">
                        <span class="title">Suscritos</span>
                        <span class="details">Usuarios de suscripción</span>
                    </a>
                    <span class="icon-thumbnail"><i class="pg-note"></i></span>
                </li>

                <li class="<?php echo activate_menu($os = array("idiomas")); ?>">
                    <a href="<?=base_url('manager/idiomas');?>" class="detailed">
                        <span class="title">Idiomas</span>
                        <span class="details">Idiomas disponibles</span>
                    </a>
                    <span class="icon-thumbnail"><i class="pg-note"></i></span>
                </li>

                <li class="<?php echo activate_menu($os = array("cotizaciones")); ?>">
                    <a href="<?=base_url('manager/cotizaciones');?>" class="detailed">
                        <span class="title">Cotizaciones</span>
                        <span class="details">Listado de cotizaciones</span>
                    </a>
                    <span class="icon-thumbnail"><i class="pg-note"></i></span>
                </li>

                <li class="<?php echo activate_menu($os = array("configuracion")); ?>">
                    <a href="<?=base_url('manager/configuracion');?>" class="detailed">
                        <span class="title">Configuración</span>
                        <span class="details">Configuración del portal</span>
                    </a>
                    <span class="icon-thumbnail"><i class="pg-note"></i></span>
                </li>

            </ul>

            <div class="clearfix"></div>

        </div>
        <!-- END SIDEBAR MENU -->
    </nav>
    <!-- END SIDEBPANEL