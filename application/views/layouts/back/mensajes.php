<?php if ($this->session->flashdata('error')): ?>
    <div class="alert alert-warning alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Error!</strong> <?= $this->session->flashdata('error') ?>
    </div>
<?php endif; ?>

<?php if ($this->session->flashdata('delete')): ?>
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Eliminar!</strong> <?= $this->session->flashdata('delete') ?>
    </div>
<?php endif; ?>

<?php if ($this->session->flashdata('create')): ?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Creado!</strong> <?= $this->session->flashdata('create') ?>
    </div>
<?php endif; ?>

<?php if ($this->session->flashdata('update')): ?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Actualizado!</strong> <?= $this->session->flashdata('update') ?>
    </div>
<?php endif; ?>