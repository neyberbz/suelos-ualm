<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>UNIVERSIDAD AGRARIA DE LA MOLINA - LOGIN</title>

        <!-- Bootstrap -->
        <link href="<?= base_url('assets') ?>/css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?= base_url('assets') ?>/css/bootstrap-datetimepicker.css" rel="stylesheet">
        <link href="<?= base_url('assets') ?>/css/ualm.css" rel="stylesheet" />
        <link href="<?= base_url('assets') ?>/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="<?= base_url('assets') ?>/summernote/css/summernote.css" rel="stylesheet" type="text/css" media="screen">
        <link href="<?= base_url('assets') ?>/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" media="screen">

        <script src="<?= base_url('assets') ?>/js/modernizr.min.js" type='text/javascript'></script>
        <script src="<?= base_url('assets') ?>/js/css3-mediaqueries.js" type='text/javascript'></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body id="backend">