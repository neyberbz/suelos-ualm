       <!-- FOOTER -->
        <footer>
            <div class="container">
                <hr>
                <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2">
                    <div class="col-xs-12 col-sm-12 col-md-4 text-center">
                        <img src="<?= base_url('assets') ?>/img/telefono.svg" class="img-center"><br>
                        614 - 7800 Anexo. 222
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 text-center">
                        <img src="<?= base_url('assets') ?>/img/correo.svg" class="img-center"><br>
                        614 - 7800 Anexo. 222
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 text-center">
                        <img src="<?= base_url('assets') ?>/img/map.svg" class="img-center"><br>
                        614 - 7800 Anexo. 222
                    </div>
                </div>
            </div>
            <div class="container">
                <hr>
                <div class="col-xs-12 col-sm-6 col-md-6 text-left">Todos los derechos reservados 2016 © Universidad la Molina</div>
                <div class="col-xs-12 col-sm-6 col-md-6 text-right">Diseño y Desarrollo por La empresa</div>
            </div>
        </footer>
        <!-- /FOOTER -->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?= base_url('assets') ?>/js/bootstrap.min.js"></script>
        <script src="<?= base_url('assets') ?>/js/sweetalert2.min.js"></script>
        <!-- <script src="<?= base_url('assets') ?>/js/ualm.js"></script> -->
        <script src="<?= base_url('assets') ?>/js/jquery.dataTables.min.js"></script>
        <script src="<?= base_url('assets') ?>/js/moment.min.js"></script>
        <script src="<?= base_url('assets') ?>/js/bootstrap-datetimepicker.js"></script>
        <script src="<?= base_url('assets') ?>/summernote/js/summernote.min.js" type="text/javascript"></script>

        <script>
            $(document).ready(function(){
                $('.table').DataTable({
                    paging:   true,
                    lengthChange: false,
                    searching: false,
                    info: false,
                    ordering:  false
                });
            });

            $('.summernote').summernote({
                height: 200,
                onfocus: function(e) {
                    $('body').addClass('overlay-disabled');
                },
                onblur: function(e) {
                    $('body').removeClass('overlay-disabled');
                },
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['view', ['fullscreen', 'codeview']]
                ]
            });

            function delet_item(item){
                swal({
                    title: 'Desea eliminar el item?',
                    text: "Confirmar si desea eliminar item!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si eliminar el item!'
                }).then(function() {
                    $(item).remove();
                    swal(
                        'Eliminado!',
                        'Tu item a sido eliminado.',
                        'success'
                    );
                })
            }

            $(function(){
                $('#datetimepicker1').datetimepicker({
                    format: 'MM/DD/YY'
                });

                $('#datetimepicker2').datetimepicker({
                    format: 'MM/DD/YY',
                    useCurrent: false //Important! See issue #1075
                });

                $('.table tbody').on( 'click', 'a.deleteE', function () {
                    var tr = $(this).parents('tr'),
                        idElemento = $(this).attr('data-id'),
                        urlDelete = $('#urlDeleteElemento').val()+idElemento;
                    swal({
                        title: 'Desea eliminar el item '+idElemento+'?',
                        text: "Confirmar si desea eliminar!",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Si eliminar el item!'
                    }).then(function() {
                        $.get(urlDelete).done(function(data){
                            var table = $('.table').DataTable();
                            table.row(tr).remove().draw();
                            swal(
                                'Eliminado!',
                                'El item '+idElemento+' a sido eliminado.',
                                'success'
                            );
                        });
                    })
                });

                $('.table tbody').on( 'click', 'a.deleteA', function () {
                    var tr = $(this).parents('tr'),
                        idAnalisis = $(this).attr('data-id'),
                        urlDelete = $('#urlDeleteAnalisis').val()+idAnalisis;
                    swal({
                        title: 'Desea eliminar el item '+idAnalisis+'?',
                        text: "Confirmar si desea eliminar!",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Si eliminar el item!'
                    }).then(function() {
                        $.get(urlDelete).done(function(data){
                            var table = $('.table').DataTable();
                            table.row(tr).remove().draw();
                            swal(
                                'Eliminado!',
                                'El item '+idAnalisis+' a sido eliminado.',
                                'success'
                            );
                        });
                    })
                });

                $("#datetimepicker1").on("dp.change", function (e) {
                    $('#datetimepicker2').data("DateTimePicker").minDate(e.date);
                });

                $("#datetimepicker2").on("dp.change", function (e) {
                    $('#datetimepicker1').data("DateTimePicker").maxDate(e.date);
                });

                $('#addElemento').bind('click', function(){
                    var url = $('#urlElemento').val(),
                        idAnalisis  = $('#formElementos').find('#idAnalisis').val(),
                        idServicio  = $('#formElementos').find('#idServicio').val(),
                        nombre      = $('#formElementos').find('#nombre_elemento').val(),
                        descripcion = $('#formElementos').find('#descripcion_elemento').val(),
                        precio      = $('#formElementos').find('#precio_elemento').val(),
                        estado      = $('#formElementos').find('input[name="estado"]').val();

                    var posting = $.post( url, {
                        idAnalisis: idAnalisis,
                        idServicio: idServicio,
                        nombre_elemento: nombre,
                        descripcion_elemento: descripcion,
                        precio_elemento: precio,
                        estado: estado
                    });
                    posting.done(function(data){
                        var text = '{ "elemento" : ['+data+']}';
                        var obj = JSON.parse(text);

                        if(obj.elemento[0].estado == 1)
                            var estado = '<span class="label label-success">Activo</span>'
                        else
                            var estado = '<span class="label label-danger">No Activo</span>'

                        var table = $('#tableElementos').DataTable();
                        table.row.add([
                            obj.elemento[0].idElemento,
                            obj.elemento[0].nombre,
                            obj.elemento[0].descripcion,
                            obj.elemento[0].precio,
                            estado,
                            "<a class='delete deleteE' data-id='"+obj.elemento[0].idElemento+"'><img src='<?= base_url("assets/img/delete.svg") ?>'></a>"
                        ]).draw();
                    },"json");
                });

                $('#addElementoNew').bind('click', function(){
                    var url = $('#urlElementoNew').val(),
                        idAnalisis  = $('#formElementos').find('#token').val(),
                        nombre      = $('#formElementos').find('#nombre_elemento').val(),
                        descripcion = $('#formElementos').find('#descripcion_elemento').val(),
                        precio      = $('#formElementos').find('#precio_elemento').val(),
                        estado      = $('#formElementos').find('input[name="estado"]').val();

                    var posting = $.post( url, {
                        idAnalisis: idAnalisis,
                        nombre_elemento: nombre,
                        descripcion_elemento: descripcion,
                        precio_elemento: precio,
                        estado: estado
                    });
                    posting.done(function(data){
                        var text = '{ "elemento" : ['+data+']}';
                        var obj = JSON.parse(text);

                        if(obj.elemento[0].estado == 1)
                            var estado = '<span class="label label-success">Activo</span>'
                        else
                            var estado = '<span class="label label-danger">No Activo</span>'

                        var table = $('#tableElementos').DataTable();
                        table.row.add([
                            obj.elemento[0].idElemento,
                            obj.elemento[0].nombre,
                            obj.elemento[0].descripcion,
                            obj.elemento[0].precio,
                            estado,
                            "<a class='delete deleteE' data-id='"+obj.elemento[0].idElemento+"'><img src='<?= base_url("assets/img/delete.svg") ?>'></a>"
                        ]).draw();
                    },"json");
                });

            });


        </script>

    </body>
</html>