<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{

    public function get_submenu($cat)
    {
        $wp_datasubmenu             =   json_decode(file_get_contents(base_url('api-rest/wp-json/wp/v2/posts?filter[category_name]='.$cat)));
        $lst_submenu                =   array();

        for($i=0;$i<count($wp_datasubmenu);$i++):
            array_push($lst_submenu, [
                "slug"              =>  $wp_datasubmenu[$i]->slug,
                "titulo"            =>  $wp_datasubmenu[$i]->title->rendered
            ]);
        endfor;

        return $lst_submenu;
    }

    /**
     * [get_servicios consulta de posts de la categoria de servicios]
     * @return [array] [listado]
     */
    public function get_master($master)
    {
        $wp_datamaster           =   json_decode(file_get_contents(base_url('api-rest/wp-json/wp/v2/posts?filter[name]='.$master)));
        $lst_master              =   array();

        for($i=0;$i<count($wp_datamaster);$i++):
            array_push($lst_master, [
                "id"                =>  $wp_datamaster[$i]->id,
                "slug"              =>  $wp_datamaster[$i]->slug,
                "titulo"            =>  $wp_datamaster[$i]->title->rendered,
                "contenido"         =>  $wp_datamaster[$i]->content->rendered,
                "img_tenor"         =>  $wp_datamaster[$i]->custom_fields->imagen_tenor->url
            ]);
        endfor;

        return $lst_master;
    }

    public function get_category($categoria)
    {
        $wp_api                     =   json_decode(file_get_contents(base_url('api-rest/wp-json/wp/v2/posts?filter[category_name]='.$categoria)));
        return $wp_api;
    }

    /**
     * [get_servicios consulta de posts de la categoria de servicios]
     * @return [array] [listado]
     */
    public function get_contactanos()
    {
        $wp_datamaster           =   json_decode(file_get_contents(base_url('api-rest/wp-json/wp/v2/posts?filter[name]=contactenos')));
        $lst_master              =   array();

        for($i=0;$i<count($wp_datamaster);$i++):
            array_push($lst_master, [
                "direccion"         =>  $wp_datamaster[$i]->custom_fields->direccion,
                "telefono"          =>  $wp_datamaster[$i]->custom_fields->telefono,
                "telefax"           =>  $wp_datamaster[$i]->custom_fields->telefax,
                "email"             =>  $wp_datamaster[$i]->custom_fields->email,
                "horario"           =>  $wp_datamaster[$i]->custom_fields->horario,
                "latlon"            =>  $wp_datamaster[$i]->custom_fields->LatLon
            ]);
        endfor;

        return $lst_master;
    }

    /**
     * [get_servicios consulta de posts de la categoria de servicios]
     * @return [array] [listado]
     */
    public function get_servicios()
    {
        $wp_dataservicios           =   json_decode(file_get_contents(base_url('api-rest/wp-json/wp/v2/posts?filter[category_name]=servicios')));
        $lst_servicios              =   array();

        for($i=0;$i<count($wp_dataservicios);$i++):
            array_push($lst_servicios, [
                "slug"              =>  $wp_dataservicios[$i]->slug,
                "titulo"            =>  $wp_dataservicios[$i]->title->rendered,
                "img_medium"        =>  $wp_dataservicios[$i]->better_featured_image->media_details->sizes->medium->source_url,
                "img_full"          =>  $wp_dataservicios[$i]->better_featured_image->source_url
            ]);
        endfor;

        return $lst_servicios;
    }

    /**
     * [get_servicio recuperando listado de servicio]
     * @param  [string] $slug [slug servicio]
     * @return [array]       [datos servicio]
     */
    public function get_nosotros_servicios($slug)
    {
        $wp_datapost                =   json_decode(file_get_contents(base_url('api-rest/wp-json/wp/v2/posts?filter[name]='.$slug)));
        $lst_post                   =   array();

        for($i=0;$i<count($wp_datapost);$i++):
            array_push($lst_post, [
                "titulo"            =>  $wp_datapost[$i]->title->rendered,
                "contenido"         =>  $wp_datapost[$i]->content->rendered,
                "img_tenor"         =>  $wp_datapost[$i]->custom_fields->imagen_tenor->url
            ]);
        endfor;

        return $lst_post;
    }

    /**
     * [get_testimonios consulta de posts de la categoria de testimonios]
     * @return [type] [description]
     */
    public function get_testimonios()
    {
        $wp_datatestimonios         =   json_decode(file_get_contents(base_url('api-rest/wp-json/wp/v2/posts?filter[category_name]=testimonios')));
        $lst_testimonios            = array();

        for($i=0; $i<count($wp_datatestimonios); $i++):
            array_push($lst_testimonios, [
                "slug"              =>  $wp_datatestimonios[$i]->slug,
                "titulo"            =>  $wp_datatestimonios[$i]->title->rendered,
                "subtitulo"         =>  $wp_datatestimonios[$i]->excerpt->rendered,
                "testimonio"        =>  $wp_datatestimonios[$i]->content->rendered
            ]);
        endfor;

        return $lst_testimonios;
    }

    public function get_sliders()
    {
        $wp_datasliders         =   json_decode(file_get_contents(base_url('api-rest/wp-json/wp/v2/posts?filter[category_name]=slider')));
        $lst_sliders            = array();

        for($i=0; $i<count($wp_datasliders); $i++):
            array_push($lst_sliders, [
                "titulo"            =>  $wp_datasliders[$i]->title->rendered,
                "contenido"         =>  $wp_datasliders[$i]->content->rendered,
                "url"               =>  $wp_datasliders[$i]->custom_fields->url_slider,
                "img_full"          =>  $wp_datasliders[$i]->better_featured_image->source_url
            ]);
        endfor;

        return $lst_sliders;
    }

}