<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User class.
 *
 * @extends CI_Controller
 */
class Dashboard extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->database();
        $this->load->library(array('session','form_validation','image_lib'));
        $this->load->helper(array('url','form','extras','jwt'));
        $this->load->model('dashboard_model');
        $this->load->model('front_model');

        date_default_timezone_set('America/Lima');

        if(!isset($_SESSION['logged_in'])) redirect('user/login');

    }

    public function index()
    {
        $data = new stdClass();

        $data->panel_title      =   'Suelos';
        $data->seccion_title    =   'Panel Principal';
        $data->template         =   'back/dashboard/index';

        $this->load->view('layouts/back/header', $data);
        $this->load->view('back/dashboard/dashboard', $data);
        $this->load->view('layouts/back/footer');
    }

    /**
     * ∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞
     * SERVICIOS
     * ∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞
     */

    public function servicios()
    {
        $data = new stdClass();

        $data->seccion_title    =   'Servicios';
        $data->template         =   'back/servicios/index';

        $data->lst              =   $this->dashboard_model->lst('servicio');

        $this->load->view('layouts/back/header', $data);
        $this->load->view('back/dashboard/dashboard', $data);
        $this->load->view('layouts/back/footer');
    }

    public function screate()
    {
        $data = new stdClass();

        $data->seccion_title    =   'Nuevo Servicio';

        $data->template         =   'back/servicios/new';

        $this->load->view('layouts/back/header', $data);
        $this->load->view('back/dashboard/dashboard', $data);
        $this->load->view('layouts/back/footer');
    }

    public function seditar($id)
    {
        $data = new stdClass();

        $data->seccion_title    =   'Editar Servicio Código:'.$id;

        $data->template         =   'back/servicios/update';

        $data->lst              =   $this->dashboard_model->lst('servicio');
        $data->get              =   $this->dashboard_model->get('servicio', 'idServicio', $id);

        $this->load->view('layouts/back/header', $data);
        $this->load->view('back/dashboard/dashboard', $data);
        $this->load->view('layouts/back/footer');
    }

    public function supdate()
    {
        // VALIDANDO CAMPOS
        $this->form_validation->set_rules('nombre', 'Nombre', 'required');
        $this->form_validation->set_rules('descripcion', 'Descripción', 'required');
        $this->form_validation->set_rules('nota', 'Nota', 'required');
        // VALIDACION DE DATOS
        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('error', 'Error al guardar datos');
            redirect('manager/servicios');
        } else {
            // RECUPERANDO ID, ID IMAGENES
            $id = $this->input->post('idServicio');
            $IdImagenPortada    = $this->input->post('imagen_portada');
            $IdImagenContenido  = $this->input->post('imagen_contenido');
            // ARRAY DE DATOS
            $up = array(
                'nombre'        =>      $this->input->post('nombre'),
                'descripcion'   =>      $this->input->post('descripcion'),
                'nota'          =>      $this->input->post('nota'),
                'estado'        =>      $this->input->post('estado')
            );
            // ACTUALIZANDO SERVICIO
            $this->dashboard_model->update('servicio',$up, 'idServicio', $id);
            $this->session->set_flashdata('update', ' Actualizado correctamente!');
            // DIRECTORIO IMAGEN DE SERVICIOS
            $dirUpload = './uploads/servicios/'.$id;
            // CONFIGURACION DE LIBRERIA UPLOAD
            $config = array(
                'upload_path' => $dirUpload,
                'allowed_types' => "jpg|png|jpge",
                'overwrite' => TRUE,
                'max_size' => "512000",
                'max_height' => "1600",
                'max_width' => "1600"
            );
            // CARGANDO LIBRERIA UPLOAD
            $this->load->library('upload', $config);
            // VERIFICANDO IMAGEN PORTADA
            if(!empty($_FILES['userfile']['name'])) {
                if($this->upload->do_upload('userfile')):
                    $img = $this->upload->data('file_name');
                    $up = array(
                        'nombre'    =>  $this->input->post('nombre'),
                        'url'       =>  $img
                    );
                    print_r($IdImagenPortada);
                    print_r($up);
                    $this->dashboard_model->update('imagen', $up, 'idImagen', $IdImagenPortada);
                    $this->session->set_flashdata('update', ' Actualizado correctamente!');
                else:
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                endif;
            }
            // VERIFICANDO IMAGEN CONTENIDO
            if(!empty($_FILES['userfile1']['name'])) {
                if($this->upload->do_upload('userfile1')):
                    $img = $this->upload->data('file_name');
                    $up = array(
                        'nombre'    =>  $this->input->post('nombre'),
                        'url'       =>  $img
                    );
                    print_r($IdImagenContenido);
                    print_r($up);
                    $this->dashboard_model->update('imagen', $up, 'idImagen', $IdImagenContenido);
                    $this->session->set_flashdata('update', ' Actualizado correctamente!');
                else:
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                endif;
            }
            //  REDIRECCIONANDO PAGINA
            redirect('manager/servicios/');
        }
    }

    public function snuevo()
    {
        // VALIDANDO CAMPOS
        $this->form_validation->set_rules('nombre', 'Nombre', 'required');
        $this->form_validation->set_rules('descripcion', 'Descripción', 'required');
        $this->form_validation->set_rules('nota', 'Nota', 'required');
        // VALIDACION DE DATOS
        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('error', 'Error al guardar datos');
            redirect('manager/servicios');
        } else {
            // ARRAY DE DATOS
            $in = array(
                'nombre'        =>      $this->input->post('nombre'),
                'slug'          =>      url_title($this->input->post('nombre')),
                'descripcion'   =>      $this->input->post('descripcion'),
                'nota'          =>      $this->input->post('nota'),
                'estado'        =>      $this->input->post('estado')
            );
            // INSERTANDO NUEVO SERVICIO
            $id = $this->dashboard_model->create('servicio',$in);
            // DIRECTORIO IMAGEN DE SERVICIOS
            $dir = './uploads/servicios/'.$id;
            // CREANDO CARPETA DE SERVICIO
            if (!file_exists($dir)) {
                error_reporting(E_ERROR);
                mkdir($dir,0777);
                chmod($dir, 0777);
                error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
            }
            // CONFIGURACION DE LIBRERIA UPLOAD
            $config = array(
                'upload_path' => $dir,
                'allowed_types' => "jpg|png|jpge",
                'overwrite' => TRUE,
                'max_size' => "512000",
                'max_height' => "1600",
                'max_width' => "1600"
            );
            // CARGANDO LIBRERIA UPLOAD
            $this->load->library('upload', $config);
            // VERIFICANDO IMAGEN PORTADA
            if(!empty($_FILES['userfile']['name'])) {
                if($this->upload->do_upload('userfile')):
                    $img = $this->upload->data('file_name');
                    // INSERTANDO LA IMAGEN
                    $in = array(
                        'url'       =>  $img
                    );
                    $id_img = $this->dashboard_model->create('imagen', $in);
                    $up = array(
                        'idImagen' => $id_img
                    );
                    $this->dashboard_model->update('servicio', $up, 'idServicio', $id);
                    $this->session->set_flashdata('update', ' Ingresado correctamente!');
                else:
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                endif;
            }
            // VERIFICANDO IMAGEN CONTENIDO
            if(!empty($_FILES['userfile1']['name'])) {
                if($this->upload->do_upload('userfile1')):
                    $img = $this->upload->data('file_name');
                    // INSERTANDO LA IMAGEN
                    $in = array(
                        'url'       =>  $img
                    );
                    $id_img = $this->dashboard_model->create('imagen', $in);
                    $up = array(
                        'idImagenPortada' => $id_img
                    );
                    $this->dashboard_model->update('servicio', $up, 'idServicio', $id);
                    $this->session->set_flashdata('update', ' Ingresado correctamente!');
                else:
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                endif;
            }
            // REDIRECCIONANDO PAGINA
            redirect('manager/servicios/');
        }
    }

    public function sdelete($id)
    {
        $this->dashboard_model->delete('servicio', 'idServicio', $id);
        echo $id;
    }

    /**
     * ∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞
     * ANÁLISIS
     * ∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞
     */

    public function analisis()
    {
        $data = new stdClass();

        $data->panel_title      =   'Suelos';
        $data->seccion_title    =   'Panel Principal';
        $data->template         =   'back/analisis/index';

        $data->lst              =   $this->dashboard_model->lst('analisis');

        $this->load->view('layouts/back/header', $data);
        $this->load->view('back/dashboard/dashboard', $data);
        $this->load->view('layouts/back/footer');
    }

    public function acreate()
    {
        $data = new stdClass();

        $data->seccion_title    =   'Crear Ánalisis';
        $data->template         =   'back/analisis/new';

        // creando token para agregar elementos
        $data->token            =   date('mdYh');

        $data->lst_servicios    =   $this->dashboard_model->lst('servicio');
        $data->lst_analisis     =   $this->dashboard_model->lst('grupo_analisis');

        $this->load->view('layouts/back/header', $data);
        $this->load->view('back/dashboard/dashboard', $data);
        $this->load->view('layouts/back/footer');
    }

    public function anew()
    {
        // validando campos
        $this->form_validation->set_rules('token', 'Token', 'required');
        $this->form_validation->set_rules('servicios', 'Servicios', 'required');
        $this->form_validation->set_rules('nombre', 'Nombre', 'required');
        $this->form_validation->set_rules('tipo', 'Tipo de Ánalisis', 'required');
        $this->form_validation->set_rules('estado', 'Estado de Ánalisis', 'required');

        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('error', 'Error al guardar datos');
            redirect('manager/analisis');
        } else {
            $token = $this->input->post('token');

            $in = array(
                'idServicio'        =>      $this->input->post('servicios'),
                'idGrupo_Analisis'  =>      $this->input->post('grupoAnalisis'),
                'nombre'            =>      $this->input->post('nombre'),
                'descripcion'       =>      $this->input->post('descripcion'),
                'precio'            =>      $this->input->post('costo'),
                'tipo'              =>      $this->input->post('tipo'),
                'estado'            =>      $this->input->post('estado')
            );
            $id = $this->dashboard_model->create('analisis', $in);

            $analisis = $this->dashboard_model->get('analisis', 'idAnalisis', $id);

            $lst_elementos = $this->dashboard_model->sql("SELECT * FROM elemento WHERE idServicio = ".$token);

            $up = array(
                'idServicio' => $analisis->idServicio,
                'idAnalisis' => $analisis->idAnalisis
            );

            foreach ($lst_elementos as $item):
                $this->dashboard_model->update('elemento', $up, 'idElemento' , $item->idElemento);
            endforeach;

            $this->session->set_flashdata('create', 'Creado correctamente!');
            redirect('manager/analisis');
        }
    }

    public function aeditar($id)
    {
        $data = new stdClass();

        $data->seccion_title    =   'Editar Ánalisis Código:'.$id;

        $data->template         =   'back/analisis/update';

        $analisis               =   $this->dashboard_model->get('analisis', 'idAnalisis', $id);

        $data->get              =   $analisis;
        $data->lst_servicios    =   $this->dashboard_model->lst('servicio');
        $data->lst_analisis     =   $this->dashboard_model->lst('grupo_analisis');
        $data->lst_elementos    =   $this->dashboard_model->sql('SELECT * FROM elemento WHERE idServicio = '.$analisis->idServicio.' AND idAnalisis = '.$analisis->idAnalisis.' AND estado = 1');

        $this->load->view('layouts/back/header', $data);
        $this->load->view('back/dashboard/dashboard', $data);
        $this->load->view('layouts/back/footer');
    }

    public function aupdate()
    {
        // validando campos
        $this->form_validation->set_rules('id', 'IdAnalisis', 'required');
        $this->form_validation->set_rules('servicios', 'Servicios', 'required');
        $this->form_validation->set_rules('nombre', 'Nombre', 'required');
        $this->form_validation->set_rules('tipo', 'Tipo de Ánalisis', 'required');
        $this->form_validation->set_rules('estado', 'Estado de Ánalisis', 'required');

        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('error', 'Error al guardar datos');
            redirect('manager/analisis');
        } else {

            $id = $this->input->post('id');

            $up = array(
                'idServicio'        =>      $this->input->post('servicios'),
                'idGrupo_Analisis'  =>      $this->input->post('grupoAnalisis'),
                'nombre'            =>      $this->input->post('nombre'),
                'descripcion'       =>      $this->input->post('descripcion'),
                'precio'            =>      $this->input->post('costo'),
                'tipo'              =>      $this->input->post('tipo'),
                'estado'            =>      $this->input->post('estado')
            );

            print_r($up);

            $this->dashboard_model->update('analisis', $up, 'idAnalisis', $id);
            $this->session->set_flashdata('update', 'Actualizado correctamente!');
            redirect('manager/analisis');
        }
    }

    public function adelete($id)
    {
        $this->dashboard_model->delete('analisis', 'idAnalisis', $id);
        echo $id;
    }

    /**
     * ELEMENTOS DE ANALISIS
     */

    public function elementoAdd()
    {
        $in = array(
            'idAnalisis'    =>      $this->input->post('idAnalisis'),
            'idServicio'    =>      $this->input->post('idServicio'),
            'nombre'        =>      $this->input->post('nombre_elemento'),
            'descripcion'   =>      $this->input->post('descripcion_elemento'),
            'precio'        =>      $this->input->post('precio_elemento'),
            'estado'        =>      $this->input->post('estado'),
        );
        $id = $this->dashboard_model->create('elemento', $in);
        echo json_encode($this->dashboard_model->get('elemento', 'idElemento', $id));
    }

    public function elementoAddNew()
    {
        $in = array(
            'idServicio'    =>      $this->input->post('idAnalisis'),
            'nombre'        =>      $this->input->post('nombre_elemento'),
            'descripcion'   =>      $this->input->post('descripcion_elemento'),
            'precio'        =>      $this->input->post('precio_elemento'),
            'estado'        =>      $this->input->post('estado'),
        );
        $id = $this->dashboard_model->create('elemento', $in);
        echo json_encode($this->dashboard_model->get('elemento', 'idElemento', $id));
    }

    public function elementoDelete($id)
    {
        $this->dashboard_model->delete('elemento', 'idElemento', $id);
        echo $id;
    }

    public function elementoDeleteNew($id)
    {
        $this->dashboard_model->delete('elemento', 'idElemento', $id);
        echo $id;
    }

    /**
     * ∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞
     * GRUPO DE ANALISIS
     * ∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞
     */

    public function grupo()
    {
        $data = new stdClass();

        $data->seccion_title    =   'Servicios';
        $data->template         =   'back/grupoanalisis/index';

        $data->lst              =   $this->dashboard_model->lst('grupo_analisis');

        $this->load->view('layouts/back/header', $data);
        $this->load->view('back/dashboard/dashboard', $data);
        $this->load->view('layouts/back/footer');
    }

    public function gcreate()
    {
        $data = new stdClass();

        $data->seccion_title    =   'Nuevo Grupo de Ánalisis';

        $data->template         =   'back/grupoanalisis/new';

        $this->load->view('layouts/back/header', $data);
        $this->load->view('back/dashboard/dashboard', $data);
        $this->load->view('layouts/back/footer');
    }

    public function geditar($id)
    {
        $data = new stdClass();

        $data->seccion_title    =   'Editar Grupo de Ánalisis Código:'.$id;

        $data->template         =   'back/grupoanalisis/update';

        $data->lst              =   $this->dashboard_model->lst('grupo_analisis');
        $data->get              =   $this->dashboard_model->get('grupo_analisis', 'idGrupo_Analisis', $id);

        $this->load->view('layouts/back/header', $data);
        $this->load->view('back/dashboard/dashboard', $data);
        $this->load->view('layouts/back/footer');
    }

    public function gupdate()
    {
        // VALIDANDO CAMPOS
        $this->form_validation->set_rules('nombre', 'Nombre', 'required');
        $this->form_validation->set_rules('descripcion', 'Descripción', 'required');
        $this->form_validation->set_rules('estado', 'Estado', 'required');
        // VALIDACION DE DATOS
        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('error', 'Error al guardar datos');
            redirect('manager/grupo_analisis');
        } else {
            // RECUPERANDO ID, ID IMAGENES
            $id = $this->input->post('idGrupoAnalisis');
            // ARRAY DE DATOS
            $up = array(
                'nombre'        =>      $this->input->post('nombre'),
                'descripcion'   =>      $this->input->post('descripcion'),
                'estado'        =>      $this->input->post('estado')
            );
            // ACTUALIZANDO SERVICIO
            $this->dashboard_model->update('grupo_analisis',$up, 'idGrupo_Analisis', $id);
            $this->session->set_flashdata('update', ' Actualizado correctamente!');
            redirect('manager/grupo_analisis/');
        }
    }

    public function gnuevo()
    {
        // VALIDANDO CAMPOS
        $this->form_validation->set_rules('nombre', 'Nombre', 'required');
        $this->form_validation->set_rules('descripcion', 'Descripción', 'required');
        $this->form_validation->set_rules('estado', 'Estado', 'required');
        // VALIDACION DE DATOS
        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('error', 'Error al guardar datos');
            redirect('manager/grupo_analisis');
        } else {
            // ARRAY DE DATOS
            $in = array(
                'nombre'        =>      $this->input->post('nombre'),
                'descripcion'   =>      $this->input->post('descripcion'),
                'estado'        =>      $this->input->post('estado')
            );
            // INSERTANDO NUEVO SERVICIO
            $id = $this->dashboard_model->create('grupo_analisis',$in);
            $this->session->set_flashdata('Create', ' Ingresado correctamente!');
            redirect('manager/grupo_analisis/');
        }
    }

    public function gdelete($id)
    {
        $this->dashboard_model->delete('grupo_analisis', 'idGrupo_Analisis', $id);
        echo $id;
    }

    /**
     * ∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞
     * FUNCIONES GLOBALES
     * ∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞
     */

    public function recovery_estado($estado)
    {
        if($estado == 1)
            $text = 'Activo';
        else
            $text = 'Inactivo';
        return $text;
    }

    /**
     * ∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞
     * USUARIO
     * ∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞•∞
     */

    /**
     * [change cambiar password]
     * @return [type] [description]
     */
    public function change()
    {
        $data = new stdClass();

        $data->panel_title      =   'Vlacar';
        $data->seccion_title    =   'Cambiar contraseña';

        $data->template         =   'back/password/index';
        $data->form             =   'back/password/update';

        $this->load->view('layouts/back/header', $data);
        $this->load->view('back/dashboard/dashboard', $data);
        $this->load->view('layouts/back/footer');
    }

    public function change_pwd()
    {

        // validando campos
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == false) {

            $this->session->set_flashdata('error', 'Error al guardar datos');
            redirect('manager/password');

        } else {

            $id = 1;

            $up = array(
                'password'           =>      $this->hash_password($this->input->post('password'))
            );

            $this->dashboard_model->update('users', $up, $id);
            $this->session->set_flashdata('update', ' Actualizado correctamente!');
            redirect('manager/password');
        }
    }

    private function hash_password($password) {

        return password_hash($password, PASSWORD_BCRYPT);

    }

    /**
     * [mupdate master update]
     * @return [type] [description]
     */
    public function mupdate()
    {

        // validando campos
        $this->form_validation->set_rules('id', 'Id', 'required');
        $this->form_validation->set_rules('key', 'Key', 'required');
        $this->form_validation->set_rules('nombre', 'Nombre', 'required');
        $this->form_validation->set_rules('descripcion', 'Descripción', 'required');

        if ($this->form_validation->run() == false) {

            $this->session->set_flashdata('error', 'Error al guardar datos');
            redirect('manager/dashboard');

        } else {

            $id = $this->input->post('id');

            $up = array(
                'key'           =>      $this->input->post('key'),
                'nombre'        =>      $this->input->post('nombre'),
                'descripcion'   =>      $this->input->post('descripcion'),
                'updated_at'    =>      date('Y-m-j')
            );

            $this->dashboard_model->update('master', $up, $id);
            $this->session->set_flashdata('update', 'Actualizado correctamente!');
            redirect('manager/dashboard');
        }
    }

    /**
     *
     * [galeria description]
     *
     * @create 28/09/2016
     * @autor bravoz.pe
     *
     */
    public function galeria()
    {
        $data = new stdClass();

        $data->panel_title      =   'Vlacar';
        $data->seccion_title    =   'Galeria de contacto';
        $data->lst              =   $this->dashboard_model->lst('contacto');

        $data->template         =   'back/galeria/index';
        $data->form             =   'back/galeria/new';

        $this->load->view('layouts/back/header', $data);
        $this->load->view('back/dashboard/dashboard', $data);
        $this->load->view('layouts/back/footer');
    }

    // public function gcreate()
    // {
    //     $in = array(
    //         'img'           =>      '',
    //         'created_at'    =>      date('Y-m-j'),
    //         'updated_at'    =>      date('Y-m-j')
    //     );

    //     // recuperandp id slider ingresada
    //     $id = $this->dashboard_model->create('contacto',$in);

    //     // consultar si existe carpeta
    //     $dir="./uploads/contacto/".$id;

    //     // creando carpeta para categoria nueva
    //     if (!file_exists($dir)) {
    //         error_reporting(E_ERROR);
    //         mkdir($dir,0777);
    //         chmod($dir, 0777);
    //         error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
    //     }

    //     // directorio upload img
    //     $dirUpload = './uploads/contacto/'.$id;

    //     // configuración de libreria upload
    //     $config = array(
    //         'upload_path' => $dirUpload,
    //         'allowed_types' => "jpg|png|jpge",
    //         'overwrite' => TRUE,
    //         'max_size' => "512000",
    //         'max_height' => "1600",
    //         'max_width' => "1600"
    //     );

    //     // load  libreria upload
    //     $this->load->library('upload', $config);

    //     // subiendo img categoria
    //     if($this->upload->do_upload('userfile')){
    //         $img = $this->upload->data('file_name');
    //         $up = array( 'img'  => $img );
    //         $this->dashboard_model->update('contacto', $up, $id);
    //         $this->session->set_flashdata('create', 'Ingresado correctamente!');

    //         // recuperando imagen guardada
    //         $dirImg     = './uploads/contacto/'.$id.'/'.$img;

    //         // generando thumbs image
    //         $thumbs = array(
    //             'image_library'     => 'gd2',
    //             'source_image'      => $dirImg,
    //             'create_thumb'      => TRUE,
    //             'maintain_ratio'    => TRUE,
    //             'width'             => 647,
    //             'height'            => 374
    //         );

    //         // load library thumbs
    //         $this->image_lib->initialize($thumbs);

    //         if ( ! $this->image_lib->resize()):
    //             $this->session->set_flashdata('error', $this->image_lib->display_errors());
    //             redirect('manager/galeria');
    //         endif;

    //         redirect('manager/galeria');
    //     }else{
    //         // error de subida
    //         $this->session->set_flashdata('error', $this->upload->display_errors());
    //         redirect('manager/galeria');
    //     }
    // }

    // public function gdelete($id)
    // {
    //     $this->dashboard_model->delete('contacto', $id);
    //     $this->session->set_flashdata('delete', 'correctamente');
    //     redirect('manager/galeria');
    // }

    /**
     *
     * [producto description]
     *
     * @create 28/09/2016
     * @autor bravoz.pe
     *
     */
    public function producto($id)
    {
        $data = new stdClass();

        if($id == 'es'): $idioma = 'Español'; else: $idioma = 'English'; endif;

        $data->panel_title      =   'Vlacar';
        $data->seccion_title    =   'Productos - ' . $idioma;
        $data->id               =   $id;
        $data->lst              =   $this->dashboard_model->lst_get('producto', 'id_pais', $id);

        $data->template         =   'back/productos/index';
        $data->form             =   'back/productos/new';

        $this->load->view('layouts/back/header', $data);
        $this->load->view('back/dashboard/dashboard', $data);
        $this->load->view('layouts/back/footer');
    }

    public function pcreate()
    {
        $data = new stdClass();

        $id_idioma = $this->input->post('id_idioma');

        // validando campos
        $this->form_validation->set_rules('titulo', 'Título', 'required');
        $this->form_validation->set_rules('descripcion', 'Descripción', 'required');
        $this->form_validation->set_rules('id_idioma', 'Idioma', 'required');

        if ($this->form_validation->run() == false) {

            $this->session->set_flashdata('error', 'Error al guardar datos');
            redirect('manager/productos');

        } else {

            $in = array(
                'titulo'        =>      $this->input->post('titulo'),
                'descripcion'   =>      $this->input->post('descripcion'),
                'img'           =>      '',
                'id_pais'       =>      $this->input->post('id_idioma'),
                'created_at'    =>      date('Y-m-j'),
                'updated_at'    =>      date('Y-m-j')
            );

            // recuperandp id slider ingresada
            $id = $this->dashboard_model->create('producto',$in);

            // consultar si existe carpeta
            $dir="./uploads/productos/".$id;

            // creando carpeta para categoria nueva
            if (!file_exists($dir)) {
                error_reporting(E_ERROR);
                mkdir($dir,0777);
                chmod($dir, 0777);
                error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
            }

            // directorio upload img
            $dirUpload = './uploads/productos/'.$id;

            // configuración de libreria upload
            $config = array(
                'upload_path' => $dirUpload,
                'allowed_types' => "jpg|png|jpge",
                'overwrite' => TRUE,
                'max_size' => "512000",
                'max_height' => "1600",
                'max_width' => "1600"
            );

            // load  libreria upload
            $this->load->library('upload', $config);

            // subiendo img categoria
            if($this->upload->do_upload('userfile')){
                $img = $this->upload->data('file_name');
                $up = array( 'img'  => $img );
                $this->dashboard_model->update('producto', $up, $id);
                $this->session->set_flashdata('create', ' Ingresado correctamente!');

                // recuperando imagen guardada
                $dirImg     = './uploads/productos/'.$id.'/'.$img;

                // generando thumbs image
                $thumbs = array(
                    'image_library'     => 'gd2',
                    'source_image'      => $dirImg,
                    'create_thumb'      => TRUE,
                    'maintain_ratio'    => TRUE,
                    'width'             => 330,
                    'height'            => 330
                );

                // load library thumbs
                $this->image_lib->initialize($thumbs);

                if ( ! $this->image_lib->resize()):
                    $this->session->set_flashdata('error', $this->image_lib->display_errors());
                    redirect('manager/productos/' . $id_idioma);
                endif;


                redirect('manager/productos/' . $id_idioma);
            }else{
                // error de subida
                $this->session->set_flashdata('error', $this->upload->display_errors());
                redirect('manager/productos/' . $id_idioma);
            }
        }
    }

    public function pedit($id_idioma, $id)
    {
        $data = new stdClass();

        $data->panel_title      =   'Vlacar';
        $data->seccion_title    =   'Editar Producto '.'<small>/ <a href>Regresar a listado</a></small>';
        $data->lst              =   $this->dashboard_model->lst_get('producto', 'id_pais', $id_idioma);
        $data->id               =   $id_idioma;
        $data->get              =   $this->dashboard_model->get_row('producto', $id);

        $data->template         =   'back/productos/index';
        $data->form             =   'back/productos/update';

        $this->load->view('layouts/back/header', $data);
        $this->load->view('back/dashboard/dashboard', $data);
        $this->load->view('layouts/back/footer');
    }

    public function pupdate()
    {
        $data = new stdClass();

        $id_idioma = $this->input->post('id_idioma');

        // validando campos
        $this->form_validation->set_rules('titulo', 'Título', 'required');
        $this->form_validation->set_rules('descripcion', 'Descripción', 'required');
        $this->form_validation->set_rules('id_idioma', 'Idioma', 'required');

        if ($this->form_validation->run() == false) {

            $this->session->set_flashdata('error', 'Error al guardar datos');
            redirect('manager/productos/'.$id_idioma);

        } else {

            $id = $this->input->post('id');

            $up = array(
                'titulo'        =>      $this->input->post('titulo'),
                'descripcion'   =>      $this->input->post('descripcion'),
                'updated_at'    =>      date('Y-m-j')
            );


            // recuperandp id slider ingresada
            $this->dashboard_model->update('producto',$up, $id);

            // directorio upload img
            $dirUpload = './uploads/productos/'.$id;

            // configuración de libreria upload
            $config = array(
                'upload_path' => $dirUpload,
                'allowed_types' => "jpg|png|jpge",
                'overwrite' => TRUE,
                'max_size' => "512000",
                'max_height' => "1600",
                'max_width' => "1600"
            );


            // load  libreria upload
            $this->load->library('upload', $config);

            // subiendo img slider
            if(!empty($_FILES['userfile']['name'])):
                if($this->upload->do_upload('userfile')):
                    $img = $this->upload->data('file_name');
                    $up = array( 'img'  => $img );
                    $this->dashboard_model->update('producto', $up, $id);
                    $this->session->set_flashdata('update', ' Actualizado correctamente!');

                    // recuperando imagen guardada
                    $dirImg     = './uploads/productos/'.$id.'/'.$img;

                    // generando thumbs image
                    $thumbs = array(
                        'image_library'     => 'gd2',
                        'source_image'      => $dirImg,
                        'create_thumb'      => TRUE,
                        'maintain_ratio'    => TRUE,
                        'width'             => 330,
                        'height'            => 330
                    );

                    // load library thumbs
                    $this->image_lib->initialize($thumbs);

                    if ( ! $this->image_lib->resize()):
                        $this->session->set_flashdata('error', $this->image_lib->display_errors());
                        redirect('manager/productos/' . $id_idioma );
                    endif;

                    redirect('manager/productos/' . $id_idioma );
                else:
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    redirect('manager/productos/' . $id_idioma );
                endif;
            else:
                $this->session->set_flashdata('update', ' Actualizado correctamente!');
                redirect('manager/productos/'.$id_idioma);
            endif;

            redirect('manager/productos/'.$id_idioma);
        }
    }

    public function pdelete($id_idioma, $id)
    {
        $this->dashboard_model->delete('producto', $id);
        $this->session->set_flashdata('delete', ' Eliminado correctamente!');
        redirect('manager/productos/' . $id_idioma);
    }

    /**
     *
     * [extracción description]
     *
     * @create 28/09/2016
     * @autor bravoz.pe
     *
     */
    public function extraccion($id)
    {
        $data = new stdClass();

        if($id == 'es'): $idioma = 'Español'; else: $idioma = 'English'; endif;

        $data->panel_title      =   'Vlacar';
        $data->seccion_title    =   'Extracción - ' . $idioma;
        $data->id               =   $id;
        $data->lst              =   $this->dashboard_model->lst_get('extraccion', 'id_pais', $id);

        $data->template         =   'back/extraccion/index';
        $data->form             =   'back/extraccion/new';

        $this->load->view('layouts/back/header', $data);
        $this->load->view('back/dashboard/dashboard', $data);
        $this->load->view('layouts/back/footer');
    }

    public function ecreate()
    {
        $data = new stdClass();

        $id_idioma = $this->input->post('id_idioma');

        // validando campos
        $this->form_validation->set_rules('titulo', 'Título', 'required');
        $this->form_validation->set_rules('descripcion', 'Descripción', 'required');
        $this->form_validation->set_rules('id_idioma', 'Idioma', 'required');

        if ($this->form_validation->run() == false) {

            $this->session->set_flashdata('error', 'Error al guardar datos');
            redirect('manager/productos');

        } else {

            $in = array(
                'titulo'        =>      $this->input->post('titulo'),
                'descripcion'   =>      $this->input->post('descripcion'),
                'id_pais'       =>      $this->input->post('id_idioma'),
                'created_at'    =>      date('Y-m-j'),
                'updated_at'    =>      date('Y-m-j')
            );

            $this->dashboard_model->create('extraccion',$in);

            $this->session->set_flashdata('create', ' Ingresado correctamente!');
            redirect('manager/extracciones/' . $id_idioma);
        }
    }

    public function eedit($id_idioma, $id)
    {
        $data = new stdClass();

        $data->panel_title      =   'Vlacar';
        $data->seccion_title    =   'Editar extraccion '.'<small>/ <a href="'. base_url('manager/extracciones/'.$id_idioma) .'">Regresar a listado</a></small>';
        $data->lst              =   $this->dashboard_model->lst_get('extraccion', 'id_pais', $id_idioma);
        $data->id               =   $id_idioma;
        $data->lst_img          =   $this->dashboard_model->sql('SELECT * FROM detalle WHERE `key` = "extraccion" AND id_producto = '.$id);
        $data->get              =   $this->dashboard_model->get_row('extraccion', $id);

        $data->template         =   'back/extraccion/index';
        $data->form             =   'back/extraccion/update';

        $this->load->view('layouts/back/header', $data);
        $this->load->view('back/dashboard/dashboard', $data);
        $this->load->view('layouts/back/footer');
    }

    public function eupdate()
    {
        $data = new stdClass();

        $id_idioma = $this->input->post('id_idioma');

        // validando campos
        $this->form_validation->set_rules('titulo', 'Título', 'required');
        $this->form_validation->set_rules('descripcion', 'Descripción', 'required');
        $this->form_validation->set_rules('id_idioma', 'Idioma', 'required');

        if ($this->form_validation->run() == false) {

            $this->session->set_flashdata('error', 'Error al guardar datos');
            redirect('manager/productos/'.$id_idioma);

        } else {

            $id = $this->input->post('id');

            $up = array(
                'titulo'        =>      $this->input->post('titulo'),
                'descripcion'   =>      $this->input->post('descripcion'),
                'updated_at'    =>      date('Y-m-j')
            );

            // recuperandp id slider ingresada
            $this->dashboard_model->update('extraccion',$up, $id);

            $this->session->set_flashdata('update', ' Actualizado correctamente!');
            redirect('manager/extracciones/'.$id_idioma);
        }
    }

    public function eaddgaleria()
    {

        $id_idioma      =   $this->input->post('id_idioma');
        $id_producto    =   $this->input->post('id');

        $in = array(
            'id_producto'   =>      $id_producto,
            'img_detalle'   =>      '',
            'key'           =>      $this->input->post('key'),
            'created_at'    =>      date('Y-m-j'),
            'updated_at'    =>      date('Y-m-j')
        );

        // recuperandp id slider ingresada
        $id = $this->dashboard_model->create('detalle',$in);

        // consultar si existe carpeta
        $dir="./uploads/extraccion/".$id;

        // creando carpeta para categoria nueva
        if (!file_exists($dir)) {
            error_reporting(E_ERROR);
            mkdir($dir,0777);
            chmod($dir, 0777);
            error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
        }

        // directorio upload img
        $dirUpload = './uploads/extraccion/'.$id;

        // configuración de libreria upload
        $config = array(
            'upload_path' => $dirUpload,
            'allowed_types' => "jpg|png|jpge",
            'overwrite' => TRUE,
            'max_size' => "512000",
            'max_height' => "1600",
            'max_width' => "1600"
        );

        // load  libreria upload
        $this->load->library('upload', $config);

        // subiendo img categoria
        if($this->upload->do_upload('userfile')){
            $img = $this->upload->data('file_name');
            $up = array( 'img_detalle'  => $img );
            $this->dashboard_model->update('detalle', $up, $id);
            $this->session->set_flashdata('create', 'Ingresado correctamente!');

            // recuperando imagen guardada
            $dirImg     = './uploads/extraccion/'.$id.'/'.$img;

            // generando thumbs image
            $thumbs = array(
                'image_library'     => 'gd2',
                'source_image'      => $dirImg,
                'create_thumb'      => TRUE,
                'maintain_ratio'    => TRUE,
                'width'             => 647,
                'height'            => 374
            );

            // load library thumbs
            $this->image_lib->initialize($thumbs);

            if ( ! $this->image_lib->resize()):
                $this->session->set_flashdata('error', $this->image_lib->display_errors());
                redirect('manager/extraccion/'.$id_idioma.'/edit/'.$id_producto);
            endif;

            redirect('manager/extraccion/'.$id_idioma.'/edit/'.$id_producto);
        }else{
            // error de subida
            $this->session->set_flashdata('error', $this->upload->display_errors());
            redirect('manager/extraccion/'.$id_idioma.'/edit/'.$id_producto);
        }
    }

    public function edeletegaleria($id_idioma, $id_producto, $id_detalle)
    {
        $this->dashboard_model->delete('detalle', $id_detalle);
        $this->session->set_flashdata('delete', ' Imagen eliminada correctamente!');
        redirect('manager/extraccion/' . $id_idioma . '/edit/' . $id_producto);
    }

    public function edelete($id_idioma, $id)
    {
        $this->dashboard_model->delete('extraccion', $id);
        $this->session->set_flashdata('delete', ' Eliminado correctamente!');
        redirect('manager/extracciones/' . $id_idioma);
    }

    /**
     *
     * [extracción description]
     *
     * @create 28/09/2016
     * @autor bravoz.pe
     *
     */
    public function produccion($id)
    {
        $data = new stdClass();

        if($id == 'es'): $idioma = 'Español'; else: $idioma = 'English'; endif;

        $data->panel_title      =   'Vlacar';
        $data->seccion_title    =   'Producción - ' . $idioma;
        $data->id               =   $id;
        $data->lst              =   $this->dashboard_model->lst_get('produccion', 'id_pais', $id);

        $data->template         =   'back/produccion/index';
        $data->form             =   'back/produccion/new';

        $this->load->view('layouts/back/header', $data);
        $this->load->view('back/dashboard/dashboard', $data);
        $this->load->view('layouts/back/footer');
    }

    public function prodcreate()
    {
        $data = new stdClass();

        $id_idioma = $this->input->post('id_idioma');

        // validando campos
        $this->form_validation->set_rules('titulo', 'Título', 'required');
        $this->form_validation->set_rules('descripcion', 'Descripción', 'required');
        $this->form_validation->set_rules('id_idioma', 'Idioma', 'required');

        if ($this->form_validation->run() == false) {

            $this->session->set_flashdata('error', 'Error al guardar datos');
            redirect('manager/productos');

        } else {

            $in = array(
                'titulo'        =>      $this->input->post('titulo'),
                'descripcion'   =>      $this->input->post('descripcion'),
                'id_pais'       =>      $this->input->post('id_idioma'),
                'created_at'    =>      date('Y-m-j'),
                'updated_at'    =>      date('Y-m-j')
            );

            $this->dashboard_model->create('produccion',$in);

            $this->session->set_flashdata('create', ' Ingresado correctamente!');
            redirect('manager/producciones/' . $id_idioma);
        }
    }

    public function prodedit($id_idioma, $id)
    {
        $data = new stdClass();

        $data->panel_title      =   'Vlacar';
        $data->seccion_title    =   'Editar produccion '.'<small>/ <a href="'. base_url('manager/producciones/'.$id_idioma) .'">Regresar a listado</a></small>';
        $data->lst              =   $this->dashboard_model->lst_get('produccion', 'id_pais', $id_idioma);
        $data->id               =   $id_idioma;
        $data->lst_img          =   $this->dashboard_model->sql('SELECT * FROM detalle WHERE `key` = "produccion" AND id_producto = '.$id);
        $data->get              =   $this->dashboard_model->get_row('produccion', $id);

        $data->template         =   'back/produccion/index';
        $data->form             =   'back/produccion/update';

        $this->load->view('layouts/back/header', $data);
        $this->load->view('back/dashboard/dashboard', $data);
        $this->load->view('layouts/back/footer');
    }

    public function produpdate()
    {
        $data = new stdClass();

        $id_idioma = $this->input->post('id_idioma');

        // validando campos
        $this->form_validation->set_rules('titulo', 'Título', 'required');
        $this->form_validation->set_rules('descripcion', 'Descripción', 'required');
        $this->form_validation->set_rules('id_idioma', 'Idioma', 'required');

        if ($this->form_validation->run() == false) {

            $this->session->set_flashdata('error', ' Error al guardar datos');
            redirect('manager/producciones/'.$id_idioma);

        } else {

            $id = $this->input->post('id');

            $up = array(
                'titulo'        =>      $this->input->post('titulo'),
                'descripcion'   =>      $this->input->post('descripcion'),
                'updated_at'    =>      date('Y-m-j')
            );

            // recuperandp id slider ingresada
            $this->dashboard_model->update('produccion',$up, $id);

            $this->session->set_flashdata('update', ' Actualizado correctamente!');
            redirect('manager/producciones/'.$id_idioma);
        }
    }

    public function prodaddgaleria()
    {

        $id_idioma      =   $this->input->post('id_idioma');
        $id_producto    =   $this->input->post('id');

        $in = array(
            'id_producto'   =>      $id_producto,
            'img_detalle'   =>      '',
            'key'           =>      $this->input->post('key'),
            'created_at'    =>      date('Y-m-j'),
            'updated_at'    =>      date('Y-m-j')
        );

        // recuperandp id slider ingresada
        $id = $this->dashboard_model->create('detalle',$in);

        // consultar si existe carpeta
        $dir="./uploads/produccion/".$id;

        // creando carpeta para categoria nueva
        if (!file_exists($dir)) {
            error_reporting(E_ERROR);
            mkdir($dir,0777);
            chmod($dir, 0777);
            error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
        }

        // directorio upload img
        $dirUpload = './uploads/produccion/'.$id;

        // configuración de libreria upload
        $config = array(
            'upload_path' => $dirUpload,
            'allowed_types' => "jpg|png|jpge",
            'overwrite' => TRUE,
            'max_size' => "512000",
            'max_height' => "1600",
            'max_width' => "1600"
        );

        // load  libreria upload
        $this->load->library('upload', $config);

        // subiendo img categoria
        if($this->upload->do_upload('userfile')){
            $img = $this->upload->data('file_name');
            $up = array( 'img_detalle'  => $img );
            $this->dashboard_model->update('detalle', $up, $id);
            $this->session->set_flashdata('create', 'Ingresado correctamente!');

            // recuperando imagen guardada
            $dirImg     = './uploads/produccion/'.$id.'/'.$img;

            // generando thumbs image
            $thumbs = array(
                'image_library'     => 'gd2',
                'source_image'      => $dirImg,
                'create_thumb'      => TRUE,
                'maintain_ratio'    => TRUE,
                'width'             => 647,
                'height'            => 374
            );

            // load library thumbs
            $this->image_lib->initialize($thumbs);

            if ( ! $this->image_lib->resize()):
                $this->session->set_flashdata('error', $this->image_lib->display_errors());
                redirect('manager/produccion/'.$id_idioma.'/edit/'.$id_producto);
            endif;

            redirect('manager/produccion/'.$id_idioma.'/edit/'.$id_producto);
        }else{
            // error de subida
            $this->session->set_flashdata('error', $this->upload->display_errors());
            redirect('manager/produccion/'.$id_idioma.'/edit/'.$id_producto);
        }
    }

    public function proddeletegaleria($id_idioma, $id_producto, $id_detalle)
    {
        $this->dashboard_model->delete('detalle', $id_detalle);
        $this->session->set_flashdata('delete', ' Imagen eliminada correctamente!');
        redirect('manager/produccion/' . $id_idioma . '/edit/' . $id_producto);
    }

    public function proddelete($id_idioma, $id)
    {
        $this->dashboard_model->delete('produccion', $id);
        $this->session->set_flashdata('delete', ' Eliminado correctamente!');
        redirect('manager/producciones/' . $id_idioma);
    }










    /**
     *
     * [slider description]
     *
     * @create 28/09/2016
     * @autor bravoz.pe
     *
     */
    // public function slider()
    // {

    //     $data = new stdClass();

    //     $data->panel_title      =   'ODA';
    //     $data->seccion_title    =   'Slider';
    //     $data->lst_idiomas      =   $this->dashboard_model->lst('oda_idiomas');
    //     $data->lst_slider       =   $this->dashboard_model->lst('oda_slider');

    //     $data->template         =   'back/slider/index';
    //     $data->form             =   'back/slider/new';

    //     $this->load->view('layouts/back/header', $data);
    //     $this->load->view('layouts/back/aside', $data);
    //     $this->load->view('back/dashboard/dashboard', $data);
    //     $this->load->view('layouts/back/footer');

    // }

    // public function screate()
    // {
    //     $data = new stdClass();

    //     // validando campos
    //     $this->form_validation->set_rules('tenor', 'Tenor', 'required');
    //     $this->form_validation->set_rules('descripcion', 'Descripción', 'required');
    //     $this->form_validation->set_rules('idioma', 'Idioma', 'required');

    //     if ($this->form_validation->run() == false) {

    //         $this->session->set_flashdata('error', 'Error al guardar datos');
    //         redirect('manager/slider');

    //     } else {

    //         $in = array(
    //             'img'           =>      '',
    //             'titulo'        =>      $this->input->post('tenor'),
    //             'descripcion'   =>      $this->input->post('descripcion'),
    //             'link'          =>      $this->input->post('link'),
    //             'id_idioma'     =>      $this->input->post('idioma'),
    //             'color'         =>      $this->input->post('color'),
    //             'orden'         =>      $this->input->post('orden'),
    //             'created_at'    =>      date('Y-m-j'),
    //             'updated_at'    =>      date('Y-m-j')
    //         );

    //         // recuperandp id slider ingresada
    //         $id = $this->dashboard_model->create('oda_slider',$in);

    //         // consultar si existe carpeta
    //         $dir="./uploads/slider/".$id;

    //         // creando carpeta para categoria nueva
    //         if (!file_exists($dir)) {
    //             error_reporting(E_ERROR);
    //             mkdir($dir,0777);
    //             chmod($dir, 0777);
    //             error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
    //         }

    //         // directorio upload img
    //         $dirUpload = './uploads/slider/'.$id;

    //         // configuración de libreria upload
    //         $config = array(
    //             'upload_path' => $dirUpload,
    //             'allowed_types' => "jpg|png",
    //             'overwrite' => TRUE,
    //             'max_size' => "512000",
    //             'max_height' => "840",
    //             'max_width' => "1500"
    //         );

    //         // load  libreria upload
    //         $this->load->library('upload', $config);

    //         // subiendo img categoria
    //         if($this->upload->do_upload('userfile')){
    //             $img = $this->upload->data('file_name');
    //             $up = array( 'img'  => $img );
    //             $this->dashboard_model->update('oda_slider', $up, $id);
    //             $this->session->set_flashdata('create', 'Ingresado correctamente!');
    //             redirect('manager/slider');
    //         }else{
    //             // error de subida
    //             $this->session->set_flashdata('error', $this->upload->display_errors());
    //             redirect('manager/slider');
    //         }
    //     }

    // }

    // public function sedit($id)
    // {
    //     $data = new stdClass();

    //     $data->panel_title      =   'ODA';
    //     $data->seccion_title    =   'Editar Slider';
    //     $data->lst_idiomas      =   $this->dashboard_model->lst('oda_idiomas');
    //     $data->lst_slider       =   $this->dashboard_model->lst('oda_slider');
    //     $data->get              =   $this->dashboard_model->get_row('oda_slider', $id);

    //     $data->template         =   'back/slider/index';
    //     $data->form             =   'back/slider/update';

    //     $this->load->view('layouts/back/header', $data);
    //     $this->load->view('layouts/back/aside', $data);
    //     $this->load->view('back/dashboard/dashboard', $data);
    //     $this->load->view('layouts/back/footer');
    // }

    // public function supdate()
    // {
    //     $data = new stdClass();

    //     // validando campos
    //     $this->form_validation->set_rules('tenor', 'Nombre', 'required');
    //     $this->form_validation->set_rules('descripcion', 'Descripción', 'required');
    //     $this->form_validation->set_rules('idioma', 'Idioma', 'required');

    //     if ($this->form_validation->run() == false) {

    //         $this->session->set_flashdata('error', 'Error al guardar datos');
    //         redirect('manager/slider');

    //     } else {

    //         $id = $this->input->post('id');

    //         $up = array(
    //             'titulo'        =>      $this->input->post('tenor'),
    //             'descripcion'   =>      $this->input->post('descripcion'),
    //             'link'          =>      $this->input->post('link'),
    //             'id_idioma'     =>      $this->input->post('idioma'),
    //             'color'         =>      $this->input->post('color'),
    //             'orden'         =>      $this->input->post('orden'),
    //             'updated_at'    =>      date('Y-m-j')
    //         );

    //         // recuperandp id slider ingresada
    //         $this->dashboard_model->update('oda_slider',$up, $id);

    //         // directorio upload img
    //         $dirUpload = './uploads/slider/'.$id;

    //         // configuración de libreria upload
    //         $config = array(
    //             'upload_path' => $dirUpload,
    //             'allowed_types' => "jpg|png|jpge",
    //             'overwrite' => TRUE,
    //             'max_size' => "512000",
    //             'max_height' => "840",
    //             'max_width' => "1500"
    //         );

    //         // load  libreria upload
    //         $this->load->library('upload', $config);

    //         // subiendo img slider
    //         if(!empty($_FILES['userfile']['name'])):
    //             if($this->upload->do_upload('userfile')):
    //                 $img = $this->upload->data('file_name');
    //                 $up = array( 'img'  => $img );
    //                 $this->dashboard_model->update('oda_slider', $up, $id);
    //                 $this->session->set_flashdata('update', 'Actualizado correctamente!');
    //                 redirect('manager/slider');
    //             else:
    //                 $this->session->set_flashdata('error', $this->upload->display_errors());
    //                 redirect('manager/slider');
    //             endif;
    //         else:
    //             $this->session->set_flashdata('update', 'Actualizado correctamente!');
    //             redirect('manager/slider');
    //         endif;
    //     }

    // }

    // public function sdelete($id)
    // {
    //     $this->dashboard_model->delete('oda_slider', $id);
    //     $this->session->set_flashdata('delete', 'Eliminado correctamente!');

    //     redirect('manager/slider');

    // }

    /**
     * [nosotros]
     *
     * @create 29/09/2016
     * @autor bravoz.pe
     *
     */
    public function nosotros()
    {
        $data = new stdClass();

        $data->panel_title      =   'ODA';
        $data->seccion_title    =   'Nosotros';
        $data->lst_idiomas      =   $this->dashboard_model->lst('oda_idiomas');

        $data->template         =   'back/nosotros/index';
        // $data->form             =   'back/nosotros/tenor';

        $this->load->view('layouts/back/header', $data);
        $this->load->view('layouts/back/aside', $data);
        $this->load->view('back/dashboard/dashboard', $data);
        $this->load->view('layouts/back/footer');
    }

    public function ncountry($id)
    {
        $data = new stdClass();

        if($id == 'es'):
            $id_categoria = '1';
        else:
            $id_categoria = '7';
        endif;

        $data->panel_title      =   'ODA';
        $data->seccion_title    =   'Nosotros ' . strtoupper($id);
        $data->get_cat          =   $this->dashboard_model->get_row('oda_categoria', $id_categoria);
        $data->slug             =   $id;
        $data->id_idioma        =   $this->front_model->recovery_country($id);
        $data->lst_idiomas      =   $this->dashboard_model->lst('oda_idiomas');
        $data->lst_post         =   $this->dashboard_model->lst_get('oda_post', 'id_categoria', $id_categoria);

        $data->template         =   'back/nosotros/country';
        // $data->form             =   'back/nosotros/new';

        $this->load->view('layouts/back/header', $data);
        $this->load->view('layouts/back/aside', $data);
        $this->load->view('back/dashboard/dashboard', $data);
        $this->load->view('layouts/back/footer');
    }

    public function nseccion($slug, $id)
    {
        $data = new stdClass();

        if($slug == 'es'):
            $id_categoria = '1';
        else:
            $id_categoria = '7';
        endif;

        $data->panel_title      =   'ODA';
        $data->seccion_title    =   'Editar seccion';
        $data->slug             =   $slug;
        $data->id_idioma        =   $this->front_model->recovery_country($id);
        $data->post             =   $this->dashboard_model->get_row('oda_post', $id);

        $data->template         =   'back/nosotros/update_seccion';

        $this->load->view('layouts/back/header', $data);
        $this->load->view('layouts/back/aside', $data);
        $this->load->view('back/dashboard/dashboard', $data);
        $this->load->view('layouts/back/footer');
    }

    public function nseccionupdate()
    {

        $data = new stdClass();

        // validando campos
        $this->form_validation->set_rules('titulo', 'Título', 'required');
        $this->form_validation->set_rules('descripcion', 'Descripción', 'required');

        if ($this->form_validation->run() == false) {

            $this->session->set_flashdata('error', 'Error al guardar datos');
            redirect('manager/slider');

        } else {

            $id     = $this->input->post('id');
            $slug   = $this->input->post('slug');

            $id = $this->input->post('id');

            $up = array(
                'titulo'        =>      $this->input->post('titulo'),
                'descripcion'   =>      $this->input->post('descripcion'),
                'updated_at'    =>      date('Y-m-j')
            );

            $this->dashboard_model->update('oda_post',$up, $id);

            // directorio upload img
            $dirUpload = './uploads/post/'.$id;

            // configuración de libreria upload
            $config = array(
                'upload_path' => $dirUpload,
                'allowed_types' => "jpg|png|jpge",
                'overwrite' => TRUE,
                'max_size' => "512000",
                'max_height' => "840",
                'max_width' => "1600"
            );

            // load  libreria upload
            $this->load->library('upload', $config);

            if(!empty($_FILES['userfile']['name'])):
                if($this->upload->do_upload('userfile')):
                    $img = $this->upload->data('file_name');
                    $up = array( 'img'  => $img );
                    $this->dashboard_model->update('oda_post', $up, $id);
                    $this->session->set_flashdata('update', 'Actualizado correctamente!');
                    redirect('manager/nosotros/'.$slug);
                else:
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    redirect('manager/nosotros/'.$slug);
                endif;
            else:
                $this->session->set_flashdata('update', 'Actualizado correctamente!');
                redirect('manager/nosotros/'.$slug);
            endif;
        }
    }

    /**
     * [ntenor_update actualizar tenor seccion nosotros por idioma]
     * @return [array] [description]
     */
    public function ntenor_update()
    {

        $id     = $this->input->post('id_categoria');
        $slug   = $this->input->post('slug');

        $up = array(
            'descripcion'   =>      $this->input->post('titulo'),
            'updated_at'    =>      date('Y-m-j')
        );

        $this->dashboard_model->update('oda_categoria',$up, $id);

        // directorio upload img
        $dirUpload = './uploads/categoria/'.$id;

        // configuración de libreria upload
        $config = array(
            'upload_path' => $dirUpload,
            'allowed_types' => "jpg|png|jpge",
            'overwrite' => TRUE,
            'max_size' => "512000",
            'max_height' => "840",
            'max_width' => "1600"
        );

        // load  libreria upload
        $this->load->library('upload', $config);

        if(!empty($_FILES['userfile']['name'])):
            if($this->upload->do_upload('userfile')):
                $img = $this->upload->data('file_name');
                $up = array( 'img'  => $img );
                $this->dashboard_model->update('oda_categoria', $up, $id);
                $this->session->set_flashdata('update', 'Actualizado correctamente!');
                redirect('manager/nosotros/'.$slug);
            else:
                $this->session->set_flashdata('error', $this->upload->display_errors());
                redirect('manager/nosotros/'.$slug);
            endif;
        else:
            $this->session->set_flashdata('update', 'Actualizado correctamente!');
            redirect('manager/nosotros/'.$slug);
        endif;

    }


    /**
     *
     * [idiomas]
     *
     * @create 28/09/2016
     * @autor bravoz.pe
     *
     */
    public function idiomas()
    {

        $data = new stdClass();

        $data->panel_title      =   'ODA';
        $data->seccion_title    =   'Idiomas';
        $data->lst_idiomas      =   $this->dashboard_model->lst('oda_idiomas');

        $data->template         =   'back/idiomas/index';
        $data->form             =   'back/idiomas/new';

        $this->load->view('layouts/back/header', $data);
        $this->load->view('layouts/back/aside', $data);
        $this->load->view('back/dashboard/dashboard', $data);
        $this->load->view('layouts/back/footer');

    }

    public function icreate()
    {
        $data = new stdClass();

        // validando campos
        $this->form_validation->set_rules('nombre', 'Nombre', 'required');
        $this->form_validation->set_rules('slug', 'Slug', 'required');

        if ($this->form_validation->run() == false) {

            $this->session->set_flashdata('error', 'Error al guardar datos');
            redirect('manager/idiomas');

        } else {

            $in = array(
                'nombre'        =>      $this->input->post('nombre'),
                'slug'          =>      $this->input->post('slug'),
                'status'        =>      1,
                'created_at'    =>      date('Y-m-j'),
                'updated_at'    =>      date('Y-m-j')
            );

            $this->dashboard_model->create('oda_idiomas',$in);
            $this->session->set_flashdata('create', 'Ingresado correctamente!');
            redirect('manager/idiomas');
        }

    }

    public function iedit($id)
    {
        $data = new stdClass();

        $data->panel_title      =   'ODA';
        $data->seccion_title    =   'Editar Idioma';
        $data->lst_idiomas      =   $this->dashboard_model->lst('oda_idiomas');
        $data->get              =   $this->dashboard_model->get_row('oda_idiomas', $id);

        $data->template         =   'back/idiomas/index';
        $data->form             =   'back/idiomas/update';

        $this->load->view('layouts/back/header', $data);
        $this->load->view('layouts/back/aside', $data);
        $this->load->view('back/dashboard/dashboard', $data);
        $this->load->view('layouts/back/footer');
    }

    public function iupdate()
    {
        $data - new stdClass();

        // validando campos
        $this->form_validation->set_rules('id', 'Id', 'required');
        $this->form_validation->set_rules('nombre', 'Nombre', 'required');
        $this->form_validation->set_rules('slug', 'Slug', 'required');

        if ($this->form_validation->run() == false) {

            $this->session->set_flashdata('error', 'Error al guardar datos');
            redirect('manager/idiomas');

        } else {

            $id = $this->input->post('id');

            $up = array(
                'nombre'        =>      $this->input->post('nombre'),
                'slug'          =>      $this->input->post('slug'),
                'updated_at'    =>      date('Y-m-j')
            );

            $this->dashboard_model->update('oda_idiomas', $up, $id);
            $this->session->set_flashdata('update', 'Actualizado correctamente!');
            redirect('manager/idiomas');
        }
    }

    public function idelete($id)
    {
        $this->dashboard_model->delete('oda_idiomas', $id);
        $this->session->set_flashdata('delete', 'Eliminado correctamente!');

        redirect('manager/idiomas');

    }


}