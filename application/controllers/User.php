<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User class.
 *
 * @extends CI_Controller
 */
class User extends CI_Controller {

	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$this->load->library(array('session'));
		$this->load->helper(array('url'));
		$this->load->model('user_model');
	}


	public function index() {}

	/**
	 * register function.
	 *
	 * @access public
	 * @return void
	 */
	public function register() {

		// create the data object
		$data = new stdClass();

		$data->lst_user = $this->user_model->lst_user();

		// load form helper and validation library
		$this->load->helper('form');
		$this->load->library('form_validation');

		// set validation rules
		$this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_numeric|min_length[4]|is_unique[users.username]', array('is_unique' => 'This username already exists. Please choose another one.'));
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
		$this->form_validation->set_rules('password_confirm', 'Confirm Password', 'trim|required|min_length[6]|matches[password]');

		if ($this->form_validation->run() === false) {

			// validation not ok, send validation errors to the view
			$this->load->view('header');
        	$this->load->view('sidebar');
			$this->load->view('user/register/register', $data);
			$this->load->view('footer');

		} else {

			// set variables from the form
			$username = $this->input->post('username');
			$email    = $this->input->post('email');
			$password = $this->input->post('password');

			if ($this->user_model->create_user($username, $email, $password)) {

				// user creation ok
				$this->load->view('header');
        		$this->load->view('sidebar');
				$this->load->view('user/register/register_success', $data);
				$this->load->view('footer');

			} else {

				// user creation failed, this should never happen
				$data->error = 'There was a problem creating your new account. Please try again.';

				// send error to the view
				$this->load->view('header');
       	 		$this->load->view('sidebar');
				$this->load->view('user/register/register', $data);
				$this->load->view('footer');

			}

		}

	}

	/**
	 * login function.
	 *
	 * @access public
	 * @return void
	 */
	public function login() {

		// create the data object
		$data = new stdClass();

		$data->page_title = 'ODA - Iniciar Sesion';

		// load form helper and validation library
		$this->load->helper('form');
		$this->load->library('form_validation');

		// set validation rules
		$this->form_validation->set_rules('username', 'Username', 'required|alpha_numeric');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == false) {

			// validation not ok, send validation errors to the view
			$this->load->view('layouts/back/login_header');
			$this->load->view('back/user/login/login', $data);
			$this->load->view('layouts/back/login_footer');

		} else {

			// set variables from the form
			$username = $this->input->post('username');
			$password = $this->input->post('password');

			if ($this->user_model->resolve_user_login($username, $password)) {

				$user_id = $this->user_model->get_user_id_from_username($username);
				$user    = $this->user_model->get_user($user_id);

				$newSession = array(
					'user_id'  		=> $user->id,
					'username' 		=> $user->username,
					'logged_in' 	=> TRUE,
					'is_confirmed' 	=> $user->is_confirmed,
					'is_admin' 		=> $user->is_admin
				);

				$this->session->set_userdata($newSession);

				redirect('manager/dashboard');

			} else {

				// login failed
				$data->error = 'Error al ingresar usuario o contraseña.';

				// send error to the view
				$this->load->view('layouts/back/login_header');
				$this->load->view('back/user/login/login', $data);
				$this->load->view('layouts/back/login_footer');

				redirect('manager/login');

			}

		}

	}

	/**
	 * logout function.
	 *
	 * @access public
	 * @return void
	 */
	public function logout() {

		// create the data object
		$data = new stdClass();

		if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true) {

			// remove session datas
			foreach ($_SESSION as $key => $value) {
				unset($_SESSION[$key]);
			}

			redirect('/manager/login');

		} else {

			// there user was not logged in, we cannot logged him out,
			// redirect him to site root
			redirect('/manager/login');

		}

	}

	public function delete(){

        $id = $this->input->post('id');
        $this->user_model->delete_user($id);

    }

}
