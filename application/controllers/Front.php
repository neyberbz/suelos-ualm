<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->library(array('session'));
        $this->load->helper(array('url'));
        $this->load->model('front_model');
        $this->load->model('dashboard_model');
	}

	public function index()
	{
        $data = new stdClass();

        $wp_datapage                =   json_decode(file_get_contents(base_url('api-rest/wp-json/')));

        $data->page_title           =   $wp_datapage->name;
        $data->page_description      =   $wp_datapage->description;

        $data->lst_servicios        =   $this->dashboard_model->lst('servicio');
        $data->lst_testimonios      =   $this->get_testimonios();
        $data->lst_sliders          =   $this->get_sliders();
        $data->contactenos          =   $this->get_contactanos();

        $this->load->view('layouts/front/head', $data);
        $this->load->view('layouts/front/header', $data);
		$this->load->view('layouts/front/slider', $data);
		$this->load->view('front/index');
		$this->load->view('layouts/front/footer');
	}

    /**
     * [servicio pagina principal de servicio]
     * @return [type] [description]
     */
    public function servicio()
    {
        $data = new stdClass();

        $wp_datapage                =   json_decode(file_get_contents(base_url('api-rest/wp-json/')));

        $data->page_title           =   'Servicios - '.$wp_datapage->name;
        $data->page_description      =   $wp_datapage->description;

        $data->submenu              =   $this->dashboard_model->lst('servicio');
        $data->servicio             =   $this->get_master('servicios');
        $data->slider_servicios     =   $this->get_servicios();

        $data->contactenos          =   $this->get_contactanos();

        $this->load->view('layouts/front/head', $data);
        $this->load->view('layouts/front/header', $data);
        $this->load->view('front/servicios');
        $this->load->view('layouts/front/footer');
    }

    public function servicios($slug)
    {
        $data = new stdClass();

        $wp_datapage                =   json_decode(file_get_contents(base_url('api-rest/wp-json/')));

        $data->page_title           =   'Servicios - ' . $wp_datapage->name;
        $data->page_description      =   $wp_datapage->description;

        $data->submenu              =   $this->get_submenu('servicios');
        $data->servicio             =   $this->get_master('servicios');
        $data->slider_servicios     =   $this->get_servicios();

        $data->contactenos          =   $this->get_contactanos();

        $this->load->view('layouts/front/head', $data);
        $this->load->view('layouts/front/header', $data);
        $this->load->view('front/servicios_post');
        $this->load->view('layouts/front/footer');
    }

    /**
     * [nosotro pagina principal de nosotros]
     * @return [type] [description]
     */
    public function nosotro()
    {
        $data = new stdClass();

        $wp_datapage                =   json_decode(file_get_contents(base_url('api-rest/wp-json/')));

        $data->page_title           =   'Nosotros - '.$wp_datapage->name;
        $data->page_description     =   $wp_datapage->description;

        $data->submenu              =   $this->get_submenu('nosotros');
        $data->master               =   $this->get_master('nosotros');
        $data->slider_servicios     =   $this->get_servicios();

        $data->contactenos          =   $this->get_contactanos();

        $this->load->view('layouts/front/head', $data);
        $this->load->view('layouts/front/header', $data);
        $this->load->view('front/nosotros');
        $this->load->view('layouts/front/footer');
    }

    /**
     * [necesitas ayuda pagina principal de necesitas ayuda]
     * @return [type] [description]
     */
    public function necesitas_ayuda()
    {
        $data = new stdClass();

        $wp_datapage                =   json_decode(file_get_contents(base_url('api-rest/wp-json/')));

        $data->page_title           =   'Necesitas ayuda - '.$wp_datapage->name;
        $data->page_description     =   $wp_datapage->description;

        $data->master               =   $this->get_master('necesitas-ayuda');
        $data->slider_servicios     =   $this->get_servicios();

        $data->contactenos          =   $this->get_contactanos();

        $this->load->view('layouts/front/head', $data);
        $this->load->view('layouts/front/header', $data);
        $this->load->view('front/ayuda');
        $this->load->view('layouts/front/footer');
    }

    /**
     * [publicaciones ayuda pagina principal de publicaciones ayuda]
     * @return [type] [description]
     */
    public function publicaciones()
    {
        $data = new stdClass();

        $wp_datapage                =   json_decode(file_get_contents(base_url('api-rest/wp-json/')));

        $data->page_title           =   'Publicaciones - '.$wp_datapage->name;
        $data->page_description     =   $wp_datapage->description;

        $data->master               =   $this->get_master('publicaciones');
        $data->slider_servicios     =   $this->get_servicios();

        $data->contactenos          =   $this->get_contactanos();

        $this->load->view('layouts/front/head', $data);
        $this->load->view('layouts/front/header', $data);
        $this->load->view('front/ayuda');
        $this->load->view('layouts/front/footer');
    }

    /**
     * [nosotros pagina interior de nosotros]
     * @param  [type] $slug [post de seccion nosotros]
     * @return [type]       [description]
     */
    public function nosotros($slug)
    {
        $data = new stdClass();

        $wp_datapage                =   json_decode(file_get_contents(base_url('api-rest/wp-json/')));

        $data->page_title           =   $wp_datapage->name . ' - Nosotros';
        $data->page_description      =   $wp_datapage->description;

        $data->submenu              =   $this->get_submenu('nosotros');
        $data->master               =   $this->get_master('nosotros');
        $data->post                 =   $this->get_nosotros_servicios($slug);
        $data->slider_servicios     =   $this->get_servicios();
        $data->slug                 =   $slug;

        $data->contactenos          =   $this->get_contactanos();

        $this->load->view('layouts/front/head', $data);
        $this->load->view('layouts/front/header', $data);
        $this->load->view('front/nosotros_post');
        $this->load->view('layouts/front/footer');
    }

    /**
     * [nosotro pagina principal de nosotros]
     * @return [type] [description]
     */
    public function faq()
    {
        $data = new stdClass();

        $wp_datapage                =   json_decode(file_get_contents(base_url('api-rest/wp-json/')));

        $data->page_title           =   'Preguntas Frecuentes - '.$wp_datapage->name;
        $data->page_description     =   $wp_datapage->description;

        $data->faq                  =   $this->get_master('faq');
        $data->master               =   $this->get_category('faq');
        $data->slider_servicios     =   $this->get_servicios();

        $data->contactenos          =   $this->get_contactanos();

        $this->load->view('layouts/front/head', $data);
        $this->load->view('layouts/front/header', $data);
        $this->load->view('front/faq');
        $this->load->view('layouts/front/footer');
    }

    /**
     * [contactenos pagina principal de nosotros]
     * @return [type] [description]
     */
    public function contactenos()
    {
        // print_r($slug);

        $data = new stdClass();

        $wp_datapage                =   json_decode(file_get_contents(base_url('api-rest/wp-json/')));

        $data->page_title           =   'Contáctenos - '.$wp_datapage->name;
        $data->page_description     =   $wp_datapage->description;

        $data->contactenos          =   $this->get_contactanos();

        $this->load->view('layouts/front/head', $data);
        $this->load->view('layouts/front/header', $data);
        $this->load->view('front/contactenos');
        $this->load->view('layouts/front/footer');
    }

	public function error_404()
	{
		$data = new stdClass();

        $wp_datapage                =   json_decode(file_get_contents(base_url('api-rest/wp-json/')));

        $data->page_title           =   $wp_datapage->name;
        $data->page_description     =   $wp_datapage->description;

        $data->error                =   'Página no encontrada';
        $data->contactenos          =   $this->get_contactanos();

		$this->load->view('layouts/front/head', $data);
        $this->load->view('layouts/front/header', $data);
        $this->load->view('front/error_404');
        $this->load->view('layouts/front/footer');
	}
}
