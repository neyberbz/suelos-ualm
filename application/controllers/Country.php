<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Country extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('front_model');
    }

    public function home($id)
    {

        $data = new stdClass();

        $country = $this->front_model->recovery_country($id);

        $data->page_title   =   'Vlacar';

        $this->load->view('layouts/front/head', $data);
        $this->load->view('front/home', $data);
        $this->load->view('layouts/front/footer', $data);

    }

}