# ************************************************************
# Sequel Pro SQL dump
# Versión 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.8-MariaDB)
# Base de datos: suelos
# Tiempo de Generación: 2016-12-08 02:53:14 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Volcado de tabla acceso
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acceso`;

CREATE TABLE `acceso` (
  `idAcceso` int(100) unsigned NOT NULL AUTO_INCREMENT,
  `idPersona` int(100) unsigned NOT NULL,
  `idUsuario` int(100) unsigned NOT NULL,
  `correo` varchar(255) NOT NULL,
  `contrasenha` varchar(255) DEFAULT NULL,
  `ultimaSession` datetime DEFAULT NULL,
  PRIMARY KEY (`idAcceso`,`idPersona`,`idUsuario`,`correo`),
  KEY `Acceso_FKIndex1` (`idUsuario`,`idPersona`,`correo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla analisis
# ------------------------------------------------------------

DROP TABLE IF EXISTS `analisis`;

CREATE TABLE `analisis` (
  `idAnalisis` int(100) unsigned NOT NULL AUTO_INCREMENT,
  `idServicio` int(100) unsigned NOT NULL,
  `idGrupo_Analisis` int(100) unsigned NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `descripcion` longtext,
  `tipo` int(10) NOT NULL,
  `precio` double NOT NULL,
  `estado` char(10) NOT NULL,
  PRIMARY KEY (`idAnalisis`,`idServicio`),
  KEY `Analisis_FKIndex1` (`idServicio`),
  KEY `Analisis_FKIndex2` (`idGrupo_Analisis`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

LOCK TABLES `analisis` WRITE;
/*!40000 ALTER TABLE `analisis` DISABLE KEYS */;

INSERT INTO `analisis` (`idAnalisis`, `idServicio`, `idGrupo_Analisis`, `nombre`, `descripcion`, `tipo`, `precio`, `estado`)
VALUES
	(1,1,0,'A.	Análisis de fertilidad de suelos','',1,0,'1'),
	(2,1,0,'B.	Análisis de caracterización ',NULL,1,0,'1'),
	(3,1,0,'C.	Análisis de Salinidad ',NULL,1,0,'1'),
	(4,1,0,'D.	Microelementos disponibles ',NULL,1,0,'1'),
	(5,1,0,'E.	Microelementos totales ',NULL,1,0,'1'),
	(6,1,0,'F.	Elementos pesados ',NULL,1,0,'1'),
	(7,1,0,'Adicionales',NULL,2,0,'1');

/*!40000 ALTER TABLE `analisis` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla cliente
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cliente`;

CREATE TABLE `cliente` (
  `idCliente` int(100) unsigned NOT NULL AUTO_INCREMENT,
  `idPersona` int(100) unsigned NOT NULL,
  `correo` varchar(255) NOT NULL,
  `empresa` varchar(255) DEFAULT NULL,
  `telefono` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idCliente`,`idPersona`,`correo`),
  KEY `Cliente_FKIndex1` (`idPersona`,`correo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla cotizacion_adicionales
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cotizacion_adicionales`;

CREATE TABLE `cotizacion_adicionales` (
  `idPedido` int(100) unsigned NOT NULL,
  `idServicio` int(100) unsigned NOT NULL,
  `idAnalisis` int(100) unsigned NOT NULL,
  `idElemento` int(100) unsigned NOT NULL,
  `idPersona` int(100) unsigned NOT NULL,
  `idCliente` int(100) unsigned NOT NULL,
  `correo` varchar(255) NOT NULL,
  `precio_unitario` double DEFAULT NULL,
  `igv` double DEFAULT NULL,
  `cantidad` int(100) unsigned DEFAULT NULL,
  `total` double DEFAULT NULL,
  PRIMARY KEY (`idPedido`,`idServicio`,`idAnalisis`,`idElemento`,`idPersona`,`idCliente`,`correo`),
  KEY `Pedido_has_Elemento_FKIndex1` (`idPedido`,`idPersona`,`idCliente`,`correo`),
  KEY `Pedido_has_Elemento_FKIndex2` (`idElemento`,`idServicio`,`idAnalisis`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla cotizacion_analisis
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cotizacion_analisis`;

CREATE TABLE `cotizacion_analisis` (
  `idPedido` int(100) unsigned NOT NULL,
  `idServicio` int(100) unsigned NOT NULL,
  `idAnalisis` int(100) unsigned NOT NULL,
  `idPersona` int(100) unsigned NOT NULL,
  `idCliente` int(100) unsigned NOT NULL,
  `correo` varchar(255) NOT NULL,
  `precio_unitario` double DEFAULT NULL,
  `igv` double DEFAULT NULL,
  `cantidad` int(100) unsigned DEFAULT NULL,
  `total` double DEFAULT NULL,
  PRIMARY KEY (`idPedido`,`idServicio`,`idAnalisis`,`idPersona`,`idCliente`,`correo`),
  KEY `Pedido_has_Analisis_FKIndex1` (`idPedido`,`idPersona`,`idCliente`,`correo`),
  KEY `Pedido_has_Analisis_FKIndex2` (`idAnalisis`,`idServicio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla elemento
# ------------------------------------------------------------

DROP TABLE IF EXISTS `elemento`;

CREATE TABLE `elemento` (
  `idElemento` int(100) unsigned NOT NULL AUTO_INCREMENT,
  `idServicio` int(100) unsigned NOT NULL,
  `idAnalisis` int(100) unsigned NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `descripcion` longtext,
  `precio` double NOT NULL,
  `estado` char(10) NOT NULL,
  PRIMARY KEY (`idElemento`,`idServicio`,`idAnalisis`),
  KEY `Elemento_FKIndex1` (`idAnalisis`,`idServicio`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8;

LOCK TABLES `elemento` WRITE;
/*!40000 ALTER TABLE `elemento` DISABLE KEYS */;

INSERT INTO `elemento` (`idElemento`, `idServicio`, `idAnalisis`, `nombre`, `descripcion`, `precio`, `estado`)
VALUES
	(1,1,7,'pH en pasta saturada.',NULL,0,'1'),
	(2,1,7,'pH relación','',0,'1'),
	(3,1,7,'Conductividad eléctrica en pasta saturada.','',0,'1'),
	(4,1,7,'Conductividad eléctrica relación 1:1 ','',0,'1'),
	(5,1,7,'Nitrógeno total.','',0,'1'),
	(6,1,7,'Fosforo disponible','',0,'1'),
	(8,1,7,'Azufre disponible.','',0,'1'),
	(9,1,7,'Carbono orgánico oxidable.','',0,'1'),
	(10,1,7,'Materia orgánica','',0,'1'),
	(11,1,7,'Carbonato de calcio','',0,'1'),
	(12,1,7,'Acidez cambiable total ','',0,'1'),
	(13,1,7,'Acidez cambiable discriminada','',0,'1'),
	(14,1,7,'Coeficientes hídricos (método de humedad equivalente): Capacidad de campo, punto de marchitez,','',0,'1'),
	(15,1,7,'Granulometría (Arena, Limo y Tamaño de Arenas)','',0,'1'),
	(16,1,7,'Textura','',0,'1'),
	(17,1,7,'Densidad aparente','',0,'1'),
	(18,1,7,'Densidad real','',0,'1'),
	(19,1,7,'Humedad gravimétrica.','',0,'1'),
	(20,1,7,'Sólidos solubles totales (SST)','',0,'1'),
	(21,1,7,'Cationes solubles (Ca2+, Mg2+, K+, Na+)','',0,'1'),
	(22,1,7,'Calcio soluble','',0,'1'),
	(23,1,7,'Magnesio soluble','',0,'1'),
	(24,1,7,'Potasio soluble','',0,'1'),
	(25,1,7,'Sodio soluble','',0,'1'),
	(26,1,7,'Aniones solubles (NO3-, CO32-, HCO3-, SO42-, Cl-)','',0,'1'),
	(27,1,7,'Nitrato soluble','',0,'1'),
	(28,1,7,'Carbonato soluble','',0,'1'),
	(29,1,7,'Bicarbonato soluble','',0,'1'),
	(30,1,7,'Sulfato soluble','',0,'1'),
	(31,1,7,'Cloruro soluble','',0,'1'),
	(32,1,7,'Boro soluble','',0,'1'),
	(33,1,7,'Yeso soluble','',0,'1'),
	(34,1,7,'Fosfatos','',0,'1'),
	(35,1,7,'Sulfatos','',0,'1'),
	(36,1,7,'Cloruros','',0,'1'),
	(37,1,7,'Nitrógeno amoniacal','',0,'1'),
	(38,1,7,'Nitrógeno nítrico','',0,'1'),
	(39,1,7,'Hierro disponible','',0,'1'),
	(40,1,7,'Cobre disponible','',0,'1'),
	(41,1,7,'Zinc disponible','',0,'1'),
	(42,1,7,'Manganeso disponible','',0,'1'),
	(43,1,7,'Boro disponible','',0,'1'),
	(44,1,7,'Hierro total','',0,'1'),
	(45,1,7,'Cobre total','',0,'1'),
	(46,1,7,'Zinc total','',0,'1'),
	(47,1,7,'Manganeso total','',0,'1'),
	(48,1,7,'Boro total ','',0,'1'),
	(49,1,7,'Plomo ','',0,'1'),
	(50,1,7,'Cadmio','',0,'1'),
	(51,1,7,'Cromo','',0,'1'),
	(52,1,7,'CIC total (en acetato de amonio)','',0,'1'),
	(53,1,7,'CIC total más cationes cambiables.','',0,'1'),
	(54,1,7,'Carbono orgánico total (con analizador TOC-L Shimadzu).','',0,'1'),
	(55,1,7,'Carbono oxidable en KMnO4','',0,'1'),
	(56,1,7,'Prueba de incubación con enmiendas (01 fuente y 03 niveles)','',0,'1'),
	(57,1,7,'Color (sistema de Munsell)','',0,'1'),
	(88,2,22,'sss','ssss',0,'1');

/*!40000 ALTER TABLE `elemento` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla grupo_analisis
# ------------------------------------------------------------

DROP TABLE IF EXISTS `grupo_analisis`;

CREATE TABLE `grupo_analisis` (
  `idGrupo_Analisis` int(100) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `estado` char(10) DEFAULT NULL,
  PRIMARY KEY (`idGrupo_Analisis`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

LOCK TABLES `grupo_analisis` WRITE;
/*!40000 ALTER TABLE `grupo_analisis` DISABLE KEYS */;

INSERT INTO `grupo_analisis` (`idGrupo_Analisis`, `nombre`, `descripcion`, `estado`)
VALUES
	(1,'A','grupo A','1'),
	(3,'B','Grupo B','1');

/*!40000 ALTER TABLE `grupo_analisis` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla imagen
# ------------------------------------------------------------

DROP TABLE IF EXISTS `imagen`;

CREATE TABLE `imagen` (
  `idImagen` int(100) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`idImagen`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

LOCK TABLES `imagen` WRITE;
/*!40000 ALTER TABLE `imagen` DISABLE KEYS */;

INSERT INTO `imagen` (`idImagen`, `nombre`, `url`)
VALUES
	(1,'analisis de aguas','analisis-aguas.jpg'),
	(2,'analasis de aguas','politica.jpg'),
	(3,'','analisis-suelos.jpg'),
	(4,'','servicio.jpg'),
	(5,'','analisis-aguas.jpg'),
	(6,'','servicio.jpg'),
	(7,'','analisis-plantas.jpg'),
	(8,'','analisis-materias.jpg'),
	(9,'','analisis-especiales.jpg'),
	(10,'','servicio.jpg'),
	(11,'','analisis-materias.jpg'),
	(12,'','politica.jpg'),
	(13,'','analisis-aguas.jpg'),
	(14,'','politica.jpg'),
	(15,'','analisis-plantas.jpg'),
	(16,'','politica.jpg'),
	(17,'','analisis-materias.jpg'),
	(18,'','politica.jpg'),
	(19,'','analisis-materias.jpg'),
	(20,'','politica.jpg'),
	(21,'','analisis-aguas.jpg'),
	(22,'','politica.jpg');

/*!40000 ALTER TABLE `imagen` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla pedido
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pedido`;

CREATE TABLE `pedido` (
  `idPedido` int(100) unsigned NOT NULL AUTO_INCREMENT,
  `idPersona` int(100) unsigned NOT NULL,
  `idCliente` int(100) unsigned NOT NULL,
  `correo` varchar(255) NOT NULL,
  `atendido` varchar(255) DEFAULT NULL,
  `comprobante` varchar(255) DEFAULT NULL,
  `numRUC` varchar(255) DEFAULT NULL,
  `fecha_registro` datetime DEFAULT NULL,
  `fecha_entrega` date DEFAULT NULL,
  `estado` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idPedido`,`idPersona`,`idCliente`,`correo`),
  KEY `Cotizacion_FKIndex1` (`idCliente`,`idPersona`,`correo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla persona
# ------------------------------------------------------------

DROP TABLE IF EXISTS `persona`;

CREATE TABLE `persona` (
  `idPersona` int(100) unsigned NOT NULL AUTO_INCREMENT,
  `correo` varchar(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `apPaterno` varchar(255) NOT NULL,
  `apMaterno` varchar(255) NOT NULL,
  `estado` char(10) DEFAULT NULL,
  PRIMARY KEY (`idPersona`,`correo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla servicio
# ------------------------------------------------------------

DROP TABLE IF EXISTS `servicio`;

CREATE TABLE `servicio` (
  `idServicio` int(100) unsigned NOT NULL AUTO_INCREMENT,
  `idImagen` int(11) unsigned NOT NULL,
  `idImagenPortada` int(11) unsigned NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `slug` varchar(60) NOT NULL,
  `descripcion` longtext,
  `nota` longtext,
  `estado` char(10) NOT NULL,
  PRIMARY KEY (`idServicio`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

LOCK TABLES `servicio` WRITE;
/*!40000 ALTER TABLE `servicio` DISABLE KEYS */;

INSERT INTO `servicio` (`idServicio`, `idImagen`, `idImagenPortada`, `nombre`, `slug`, `descripcion`, `nota`, `estado`)
VALUES
	(1,1,2,'suelos','suelos','Se analizan muestras obtenidas de capas superficiales o subsuperficiales de suelos de tierras agrícolas, pasturas, bosques o eriazos. Las muestras deben ser obtenidas en su condición de campo y no deben estar mezcladas con materiales orgánicos ni inorgánicos. Para tierras preparadas o sustratos empleados en viveros e invernaderos, dirigirse a SUSTRATOS.','<p>El tiempo de entrega de resultados puede variar dependiendo del número de muestras y de la naturaleza de las mismas</p>\r\n<p>Si algunas determinaciones o análisis no estuvieran incluidas en la presente lista, consultar ante la jefatura del laboratorio de análisis de suelos, plantas, aguas y fertilizantes.</p>\r\n<p>También se determinan adicionalmente propiedades físicas y químicas </p>','1'),
	(2,3,4,'sustratos','sustratos','Un sustrato es todo material sólido distinto del suelo, natural, de síntesis o residual, mineral u orgánico. Los sustratos, corresponden a mezclas de tierras, materia orgánica y otros componentes con más de 15% de materia orgánica.','<p>El tiempo de entrega de resultados puede variar dependiendo del número de muestras y de la naturaleza de las mismas</p>\r\n<p>Si algunas determinaciones o análisis no estuvieran incluidas en la presente lista, consultar ante la jefatura del laboratorio de análisis de suelos, plantas, aguas y fertilizantes.</p>\r\n<p>También se determinan adicionalmente propiedades físicas y químicas </p>','1'),
	(3,5,6,'B','aguas-de-riego','Grupo B','<p>El tiempo de entrega de resultados puede variar dependiendo del número de muestras y de la naturaleza de las mismas</p>\r\n<p>Si algunas determinaciones o análisis no estuvieran incluidas en la presente lista, consultar ante la jefatura del laboratorio de análisis de suelos, plantas, aguas y fertilizantes.</p>\r\n<p>También se determinan adicionalmente propiedades físicas y químicas </p>','1'),
	(4,7,8,'soluciones fertirriego','soluciones-fertirriego','Corresponden a aguas de riego que contienen fertilizantes disueltos, obtenidas de los tanques de preparación (soluciones madre) como de los laterales de riego o emisores.','<p>El tiempo de entrega de resultados puede variar dependiendo del número de muestras y de la naturaleza de las mismas</p>\r\n<p>Si algunas determinaciones o análisis no estuvieran incluidas en la presente lista, consultar ante la jefatura del laboratorio de análisis de suelos, plantas, aguas y fertilizantes.</p>\r\n<p>También se determinan adicionalmente propiedades físicas y químicas </p>','1'),
	(5,9,10,'tejidos vegetales','tejidos-vegetales','El análisis de tejido vegetal es fundamental por ser la representación del estado nutricional del suelo, pues nos da una radiografía de la planta (N, P, K, Ca, Mg, S, Fe, Mn, Cu, Zn y B) y como tal es un complemento necesario y se diría indispensable para evaluar y dar una recomendación más integral del manejo de los suelos en relación a la nutrición.\r\n\r\nAnálisis foliares y de tejidos vegetales\r\n','<p>El tiempo de entrega de resultados puede variar dependiendo del número de muestras y de la naturaleza de las mismas</p>\r\n<p>Si algunas determinaciones o análisis no estuvieran incluidas en la presente lista, consultar ante la jefatura del laboratorio de análisis de suelos, plantas, aguas y fertilizantes.</p>\r\n<p>También se determinan adicionalmente propiedades físicas y químicas </p>','1'),
	(6,11,12,'materias orgánicas','materias-organicas','-','<p>El tiempo de entrega de resultados puede variar dependiendo del número de muestras y de la naturaleza de las mismas</p>\r\n<p>Si algunas determinaciones o análisis no estuvieran incluidas en la presente lista, consultar ante la jefatura del laboratorio de análisis de suelos, plantas, aguas y fertilizantes.</p>\r\n<p>También se determinan adicionalmente propiedades físicas y químicas </p>','1'),
	(7,13,14,'fertilizantes y afines','fertilizantes-y-afines','El análisis químico de los fertilizantes tiene mucha importancia, pues conviene saber las proporciones de los compuestos fertilizantes. Por su naturaleza los fertilizantes pueden ser; fertilizantes orgánicos naturales (origen animal y vegetal), fertilizantes químicos (origen inorgánico u orgánico sintético) y fertilizantes orgánicos reforzados (mezcla de fertilizantes naturales y químicos).','Si alguno de los elementos anteriores se encuentra como elemento acompañante (en conce4ntraciones de ppm o mg/kg) S/.20.00 por elemento. Si excediera en su concentración (mayor del 2%), se reajustara según tarifa anterior.','1'),
	(8,15,16,'microbiológico','microbiologico','El suelo es un ecosistema que contiene una gran variedad de poblaciones microbianas cuyos miembros representan muchos tipos fisiológicos. Las características químicas, físicas y biológicas de un suelo particular, así como la presencia de crecimiento de plantas influirán en el número y actividades de sus diversos componentes microbianos. La comunidad microbiana en suelos es importante por su relación con la fertilidad del suelo y con los ciclos biogeoquímicos de los elementos, además del uso potencial de los miembros específicos para aplicaciones ambientales e industriales. Por lo que existe la necesidad de conocer, enumerar y aislar los miembros principales y menores de la comunidad microbiana en suelos.','<p>El tiempo de entrega de resultados puede variar dependiendo del número de muestras y de la naturaleza de las mismas</p>\r\n<p>Si algunas determinaciones o análisis no estuvieran incluidas en la presente lista, consultar ante la jefatura del laboratorio de análisis de suelos, plantas, aguas y fertilizantes.</p>\r\n<p>También se determinan adicionalmente propiedades físicas y químicas </p>','1');

/*!40000 ALTER TABLE `servicio` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `avatar` varchar(255) DEFAULT 'default.jpg',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `is_admin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_confirmed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `username`, `email`, `password`, `avatar`, `created_at`, `updated_at`, `is_admin`, `is_confirmed`, `is_deleted`)
VALUES
	(1,'admin','admin@gmail.com','$2y$10$Wi9phjgkRor.aVunEk3L1OZcTTL30Tp92BDAULd.041HpkN3BfnS.','default.jpg','2016-07-06 12:19:07','2016-07-06 12:19:10',1,1,0),
	(5,'demo1','enjey11@gmail.com','$2a$06$vwpcyLuJTdEe3yMMGq8Jj.UPCjkdWYNQjqk.VZTRJPTlnsnm5E7.2','default.jpg','2016-08-11 18:09:01',NULL,0,0,0),
	(6,'demo2','corro@gmc.com','$2a$06$vwpcyLuJTdEe3yMMGq8Jj.UPCjkdWYNQjqk.VZTRJPTlnsnm5E7.2','default.jpg','2016-08-11 18:09:41',NULL,0,0,0),
	(7,'demo3','demo3@gmail.com','$2a$06$vwpcyLuJTdEe3yMMGq8Jj.UPCjkdWYNQjqk.VZTRJPTlnsnm5E7.2','default.jpg','2016-08-11 18:29:26',NULL,0,0,0);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla usuario
# ------------------------------------------------------------

DROP TABLE IF EXISTS `usuario`;

CREATE TABLE `usuario` (
  `idUsuario` int(100) unsigned NOT NULL AUTO_INCREMENT,
  `idPersona` int(100) unsigned NOT NULL,
  `correo` varchar(255) NOT NULL,
  `rol` char(10) NOT NULL,
  PRIMARY KEY (`idUsuario`,`idPersona`,`correo`),
  KEY `Usuario_FKIndex1` (`idPersona`,`correo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
