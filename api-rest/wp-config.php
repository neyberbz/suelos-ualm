<?php
/**
 * Configuración básica de WordPress.
 *
 * El script de creación utiliza este fichero para la creación del fichero wp-config.php durante el
 * proceso de instalación. Usted no necesita usarlo en su sitio web, simplemente puede guardar este fichero
 * como "wp-config.php" y completar los valores necesarios.
 *
 * Este fichero contiene las siguientes configuraciones:
 *
 * * Ajustes de MySQL
 * * Claves secretas
 * * Prefijo de las tablas de la Base de Datos
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solcite esta información a su proveedor de alojamiento web. ** //
/** El nombre de la base de datos de WordPress */
define('DB_NAME', 'ualm');

/** Nombre de usuario de la base de datos de MySQL */
define('DB_USER', 'root');

/** Contraseña del usuario de la base de datos de MySQL */
define('DB_PASSWORD', '');

/** Nombre del servidor de MySQL (generalmente es localhost) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para usar en la creación de las tablas de la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** El tipo de cotejamiento de la base de datos. Si tiene dudas, no lo modifique. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autenticación y salts.
 *
 * ¡Defina cada clave secreta con una frase aleatoria distinta!
 * Usted puede generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress.org}
 * Usted puede cambiar estos valores en cualquier momento para invalidar todas las cookies existentes. Esto obligará a todos los usuarios a iniciar sesión nuevamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'mH&l%0#i>Oq>?q@/_SI^KoPBAuIb2QOKpD5Guzeyx$?:3l*SyFtQ^#:lwN5o(7^I');
define('SECURE_AUTH_KEY',  '.EItg=p omaN8dl0lN{uRap!D3IYFnh,(-/9,9tud+:{[iW_~?Lrde!Zx!(NXgdK');
define('LOGGED_IN_KEY',    '+?Oo*K=eSV#g>Z7z<eZ2n6EU$pd5HK]zipItx@QNq?!wwE@8Mbq+DSIpzYR35j2}');
define('NONCE_KEY',        'iN{r[yZ!wI@Fw;}n1qu`.$!3-i*n;6{~d_92x$&kMBO]zlk6c*Ub8g~>~TBwF=pf');
define('AUTH_SALT',        '/pp %f?tvv;w%-Q&6yu@d.^t4^/,9]61$@15F7f<:_7ob2ArDauox?QZ.ZR3E@TG');
define('SECURE_AUTH_SALT', '-[8xs4s5.SBv!29N>kRtZ OFnObRsB4-QgXBj-i?<ml{1nWwEaU_NWqv;T;)YD9n');
define('LOGGED_IN_SALT',   ')CS?yE<. Rx[UZxy-oJ#ZSG)tWMf0C(LsBG9Aql3rSPKm_9vWg*i*)jM}|7[za~%');
define('NONCE_SALT',       '+q9ZNobu? Q?Q+HF&f|d{Fdv_egBXN5)t64oo7`u[YRO) -|*iKr>.QI ]v?|rzC');

/**#@-*/

/**
 * Prefijo de las tablas de la base de datos de WordPress.
 *
 * Usted puede tener múltiples instalaciones en una sóla base de datos si a cada una le da 
 * un único prefijo. ¡Por favor, emplee sólo números, letras y guiones bajos!
 */
$table_prefix  = 'wp_';

/**
 * Para los desarrolladores: modo de depuración de WordPress.
 *
 * Cambie esto a true para habilitar la visualización de noticias durante el desarrollo.
 * Se recomienda encarecidamente que los desarrolladores de plugins y temas utilicen WP_DEBUG
 * en sus entornos de desarrollo.
 *
 * Para obtener información acerca de otras constantes que se pueden utilizar para la depuración, 
 * visite el Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deje de editar! Disfrute de su sitio. */

/** Ruta absoluta al directorio de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Establece las vars de WordPress y los ficheros incluidos. */
require_once(ABSPATH . 'wp-settings.php');
